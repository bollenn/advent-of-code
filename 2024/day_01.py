import argparse
import os


def part_1(lines):
    left = []
    right = []
    for line in lines:
        items = line.split()
        left.append(int(items[0]))
        right.append(int(items[1]))

    left.sort()
    right.sort()

    distance = []
    for index, item in enumerate(left):
        distance.append(abs(item - right[index]))

    return sum(distance)


def part_2(lines):
    left = []
    right = []
    for line in lines:
        items = line.split()
        left.append(int(items[0]))
        right.append(int(items[1]))

    similarity_score = 0
    for item in left:
        similarity_score += item * right.count(item)

    return similarity_score


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
