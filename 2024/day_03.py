import argparse
import os
import re


def part_1(lines):
    muls = []
    for line in lines:
        muls.extend(re.findall(r'mul\((\d*),(\d*)\)', line))
    result = 0
    for mul in muls:
        result += int(mul[0]) * int(mul[1])
    return result


def part_2(lines):
    muls = []
    for line in lines:
        muls.extend(re.findall(r'(mul\((\d*),(\d*)\)|don\'t\(\)|do\(\))', line))
    result = 0
    mul_enabled = True
    for mul in muls:
        if mul_enabled and 'mul' in mul[0]:
            result += int(mul[1]) * int(mul[2])
        if 'do()' in mul[0]:
            mul_enabled = True
        if 'don\'t()' in mul[0]:
            mul_enabled = False
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
