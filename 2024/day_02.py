import argparse
import os
import copy


def check_report(report):
    increasing = None
    for index, item in enumerate(report[:-1]):
        if increasing is None:
            increasing = report[index+1] > item
        if report[index+1] > item and not increasing:
            return False
        if report[index+1] < item and increasing:
            return False
        if not 1 <= abs(report[index+1] - item) <= 3:
            return False
    return True


def part_1(reports):
    safe_reports = 0
    for report in reports:
        if check_report(report):
            safe_reports += 1
    return safe_reports


def part_2(reports):
    safe_reports = 0
    for report in reports:
        if check_report(report):
            safe_reports += 1
        else:
            for index in range(len(report)):
                new_report = copy.copy(report)
                new_report.pop(index)
                if check_report(new_report):
                    safe_reports += 1
                    break
    return safe_reports


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    reports = []
    for line in lines:
        reports.append([int(item) for item in line.split()])

    if args.part == 1:
        result = part_1(reports)
        print(f"part1 results in {result}")
    else:
        result = part_2(reports)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
