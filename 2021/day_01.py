import argparse
import os


def part_1(all_numbers):
    increased = 0
    previous = all_numbers[0]
    for number in all_numbers[1:]:
        if number > previous:
            increased += 1
        previous = number
    return increased


def part_2(all_numbers):
    averaged = []
    for number in range(len(all_numbers)-2):
        averaged.append(sum(all_numbers[number:number+3]))
    increased = 0
    previous = averaged[0]
    for number in averaged[1:]:
        if number > previous:
            increased += 1
        previous = number
    return increased


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    all_numbers = []
    for numline in lines:
        all_numbers.append(int(numline))

    if args.part == 1:
        result = part_1(all_numbers)
        print(f"part1 results in {result}")
    else:
        result = part_2(all_numbers)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
