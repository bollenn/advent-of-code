import argparse
import os


class Race:
    def __init__(self, time, record_dist):
        self.time = time
        self.record_dist = record_dist

    def get_hold_time(self):
        min_win = None
        for i in range(self.time):
            if (self.time - i) * i > self.record_dist:
                min_win = i
                break
        max_win = None
        for i in reversed(range(self.time)):
            if (self.time - i) * i > self.record_dist:
                max_win = i
                break
        return max_win - min_win + 1


def part_1(races):
    result = 1
    for race in races:
        result *= race.get_hold_time()
    return result


def part_2(race):
    return race.get_hold_time()


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    times = []
    distances = []
    for line in lines:
        if line.find("Time:") >= 0:
            times = [int(nr) for nr in line.replace("Time:", "").split()]
            long_time = int(line.replace("Time:", "").replace(" ", ""))
        if line.find("Distance:") >= 0:
            distances = [int(nr) for nr in line.replace("Distance:", "").split()]
            long_distance = int(line.replace("Distance:", "").replace(" ", ""))

    races = []
    for i, time in enumerate(times):
        races.append(Race(time, distances[i]))

    if args.part == 1:
        result = part_1(races)
    else:
        result = part_2(Race(long_time, long_distance))

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
