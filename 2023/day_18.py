import argparse
import os
import re
from collections import namedtuple

Digger = namedtuple("Digger", ["direction", "length", "color"])

DIRECTIONS = {
    "R": [1, 0],
    "D": [0, 1],
    "L": [-1, 0],
    "U": [0, -1],
}

CODE_2_DIR = [
    "R",
    "D",
    "L",
    "U",
]


class DiggedMap:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.vectors = []

    def add_dig_line(self, direction, length):
        next_x = self.x + (length * DIRECTIONS[direction][0])
        next_y = self.y + (length * DIRECTIONS[direction][1])
        self.vectors.append([[self.x, self.y], [next_x, next_y]])
        self.x = next_x
        self.y = next_y

    def calc_area(self):
        area = 0
        outer = 0
        # use shoelac formula for area calculation
        for vector in self.vectors:
            outer += abs(vector[1][0] - vector[0][0]) + abs(vector[1][1] - vector[0][1])
            area += vector[0][0] * vector[1][1] - vector[1][0] * vector[0][1]
        return int(area / 2 + outer / 2 + 1)


def decode_color(color):
    return Digger(CODE_2_DIR[int(color[-1])], int(color[:-1], 16), "")


def part_1(dig_plan):
    dig_map = DiggedMap()
    for digger in dig_plan:
        dig_map.add_dig_line(digger.direction, digger.length)
    return dig_map.calc_area()


def part_2(dig_plan):
    dig_map = DiggedMap()
    for digger in dig_plan:
        decoded_digger = decode_color(digger.color)
        dig_map.add_dig_line(decoded_digger.direction, decoded_digger.length)
    return dig_map.calc_area()


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    dig_plan = []
    for line in lines:
        line_re = re.search(r"(\w)\s(\d*)\s\(#(.*)\)", line)
        dig_plan.append(Digger(line_re.group(1), int(line_re.group(2)), line_re.group(3)))

    if args.part == 1:
        result = part_1(dig_plan)
    else:
        result = part_2(dig_plan)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
