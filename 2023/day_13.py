import argparse
import os
from collections import namedtuple

Pattern = namedtuple("Pattern", ["rows", "columns"])


def find_first_mirror(col_row, part2=False):
    # find first mirror
    first_mirror = 0
    for row_nr, _ in enumerate(col_row):
        if row_nr == 0:
            continue
        low_part = col_row[:row_nr]
        high_part = col_row[row_nr:]
        high_pos = 0
        ok = True
        smudged = False
        for line in reversed(low_part):
            if line != high_part[high_pos]:
                if part2 and not smudged:
                    # try fixing
                    diff = sum(i != j for i, j in zip(line, high_part[high_pos]))
                    if diff == 1:
                        smudged = True
                    else:
                        ok = False
                        break
                else:
                    ok = False
                    break
            high_pos += 1
            if high_pos >= len(high_part):
                break
        if ok and (not part2 or smudged):
            first_mirror = row_nr
            break
    return first_mirror


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    patterns = []
    rows = []
    columns = []
    for line in lines:
        if len(line.strip()) == 0:
            patterns.append(Pattern(rows, columns))
            rows = []
            columns = []
            continue
        rows.append(list(line.strip()))
        for char_nr, char in enumerate(line.strip()):
            if len(columns) < char_nr + 1:
                columns.append([])
            columns[char_nr].append(char)
    patterns.append(Pattern(rows, columns))

    result = 0
    for pattern in patterns:
        first_row_mirror = find_first_mirror(pattern.rows, args.part != 1)
        first_col_mirror = find_first_mirror(pattern.columns, args.part != 1)
        result += first_row_mirror * 100 + first_col_mirror

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
