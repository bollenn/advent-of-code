import argparse
import os
import re

PART1_MAX = {
    "red": 12,
    "green": 13,
    "blue": 14,
}


def get_max_colors_per_game(lines):
    games = []
    for line in lines:
        game_nr, colors = re.findall(r"Game\s+(\d+):\s+(.*)", line)[0]
        max_color = {
            "red": 0,
            "green": 0,
            "blue": 0
        }
        for count, color in re.findall(r"(\d+)\s+(\w+)[,;]?", colors):
            if int(count) > max_color[color]:
                max_color[color] = int(count)
        games.append([int(game_nr), max_color])
    return games


def part_1(games):
    result = 0
    for game_nr, colors in games:
        if colors["red"] <= PART1_MAX["red"] and colors["green"] <= PART1_MAX["green"] and colors["blue"] <= PART1_MAX["blue"]:
            result += game_nr
    return result


def part_2(games):
    result = 0
    for _, colors in games:
        power = colors["red"] * colors["green"] * colors["blue"]
        result += power
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    games = get_max_colors_per_game(lines)

    if args.part == 1:
        result = part_1(games)
    else:
        result = part_2(games)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
