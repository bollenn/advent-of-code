import argparse
import os
import re


class Card:
    def __init__(self, entry):
        card_re = re.findall("Card\s+(\d+): (.*)\s+[|]\s+(.*)", entry)[0]
        self.id = card_re[0]
        self.winning = [int(number) for number in card_re[1].split()]
        self.numbers = [int(number) for number in card_re[2].split()]
        self.nr_available = 1

    def __str__(self):
        return f"Card {self.id} ({self.nr_available}x) with numbers {self.numbers} and winning numbers {self.winning}"

    def get_winning_nrs(self):
        win_nrs = 0
        for number in self.numbers:
            if number in self.winning:
                win_nrs += 1
        return win_nrs

    def get_points(self):
        point_val = 0
        for number in self.numbers:
            if number in self.winning:
                point_val = 1 if point_val == 0 else point_val * 2
        return point_val


def part_1(cards):
    result = 0
    for card in cards:
        result += card.get_points()
    return result


def part_2(cards):
    # get card coint
    result = 0
    for nr, card in enumerate(cards):
        next_cards = card.get_winning_nrs()
        for i in range(nr+1, nr+1+next_cards):
            if i < len(cards):
                cards[i].nr_available += card.nr_available
        result += card.nr_available
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    cards = []
    for line in lines:
        cards.append(Card(line))

    if args.part == 1:
        result = part_1(cards)
    else:
        result = part_2(cards)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
