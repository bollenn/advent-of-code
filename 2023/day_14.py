import argparse
import os


def columns_print(columns):
    for column in columns:
        print("".join(char for char in column))
    print()


def columns_transpose(columns):
    return list(map(list, zip(*columns)))


def columns_sort(columns, reverse=False):
    for column in columns:
        column_sort(column, reverse)


def column_sort(column, reverse):
    sorting = True
    while sorting:
        sorting = False
        for item_nr, item in enumerate(column[:-1]):
            if reverse:
                if column[item_nr + 1] == ".":
                    if item == "O":
                        column[item_nr + 1] = item
                        column[item_nr] = "."
                        sorting = True
            else:
                if item == ".":
                    if column[item_nr + 1] == "O":
                        column[item_nr] = column[item_nr + 1]
                        column[item_nr + 1] = "."
                        sorting = True


def count_load(columns):
    # we need north load so transpose again
    columns_to_count = columns_transpose(columns)
    load = 0
    for column in columns_to_count:
        for item_nr, item in enumerate(column):
            if item == "O":
                load += len(column) - item_nr
    return load


def part_1(columns):
    columns = columns_transpose(columns)
    columns_sort(columns)
    columns = columns_transpose(columns)
    return count_load(columns)


def part_2(columns):
    # run for a while
    loads = []
    for _ in range(500):
        # North
        columns = columns_transpose(columns)
        columns_sort(columns)
        columns = columns_transpose(columns)
        # West
        columns_sort(columns)
        # South
        columns = columns_transpose(columns)
        columns_sort(columns, True)
        columns = columns_transpose(columns)
        # East
        columns_sort(columns, True)
        loads.append(count_load(columns))
    # search for pattern start
    pattern_start = None
    for i in range(len(loads)//2):
        for y in range(i+3, len(loads)//2):
            if loads[i:i+3] == loads[y:y+3]:
                pattern_start = i
                break
        if pattern_start is not None:
            break
    # search for pattern length
    pattern_len = None
    for i in range(pattern_start + 1, len(loads)):
        if loads[pattern_start:pattern_start+i] == loads[pattern_start+i:pattern_start+i+i]:
            pattern_len = i
            break
    # calculate result
    return loads[pattern_start + ((1000000000 - pattern_start - 1) % pattern_len)]


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    columns = []
    for line in lines:
        columns.append(list(line.strip()))

    if args.part == 1:
        result = part_1(columns)
    else:
        result = part_2(columns)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
