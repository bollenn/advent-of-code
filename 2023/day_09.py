import argparse
import os


class History:
    def __init__(self, line):
        self.numbers = []
        self.numbers.append([int(nr) for nr in line.strip().split()])

    def __str__(self):
        retval = ""
        for line in self.numbers:
            retval += "".join(f" {i}" for i in line)
            retval += "\n"
        return retval

    def gen_sub_lines(self):
        all_zeros = True
        extra_nr = []
        for pos, number in enumerate(self.numbers[-1][0:-1]):
            extra_nr.append(self.numbers[-1][pos+1] - number)
            if extra_nr[-1] != 0:
                all_zeros = False
        self.numbers.append(extra_nr)
        if not all_zeros:
            self.gen_sub_lines()

    def calc_nexts(self):
        last = 0
        for line in reversed(self.numbers):
            last = last + line[-1]
            line.append(last)

    def calc_befores(self):
        last = 0
        for line in reversed(self.numbers):
            last = line[0] - last
            line.insert(0, last)

    def get_last(self):
        return self.numbers[0][-1]

    def get_first(self):
        return self.numbers[0][0]


def part_1(histories):
    result = 0
    for history in histories:
        history.gen_sub_lines()
        history.calc_nexts()
        result += history.get_last()
    return result


def part_2(histories):
    result = 0
    for history in histories:
        history.gen_sub_lines()
        history.calc_befores()
        result += history.get_first()
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    histories = []
    for line in lines:
        histories.append(History(line))

    if args.part == 1:
        result = part_1(histories)
    else:
        result = part_2(histories)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
