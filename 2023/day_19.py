import argparse
import os
import re
import copy


class Workflow:
    def __init__(self, name, line):
        self.name = name
        rule_items = line.split(",")
        self.rules = []
        for rule_item in rule_items:
            re_rule = re.match(r"(\w+)([<>])(\d+):(\w+)", rule_item)
            if re_rule is not None:
                self.rules.append([re_rule.group(1), re_rule.group(2), int(re_rule.group(3)), re_rule.group(4)])
            else:
                self.rules.append([rule_item])

    def __str__(self):
        return f"Workflow {self.name} with work {self.rules}"


class Rating:
    def __init__(self, line):
        re_line = re.match(r"{x=(\d+),m=(\d+),a=(\d+),s=(\d+)}", line)
        self.values = {}
        self.values["x"] = int(re_line.group(1))
        self.values["m"] = int(re_line.group(2))
        self.values["a"] = int(re_line.group(3))
        self.values["s"] = int(re_line.group(4))

    def __str__(self):
        return f"Rating with x={self.values['x']} m={self.values['m']} a={self.values['a']} s={self.values['s']}"

    def calculate(self, workflows):
        cur_wf = workflows["in"]
        while True:
            for rule in cur_wf.rules:
                result = None
                if len(rule) > 1:
                    if (rule[1] == "<" and self.values[rule[0]] < rule[2]) or (rule[1] == ">" and self.values[rule[0]] > rule[2]):
                        result = rule[3]
                else:
                    result = rule[0]
                if result is not None:
                    if result in ("A", "R"):
                        return result
                    cur_wf = workflows[result]
                    break

    def find_ranges(self, workflows, cur_wf_name, ranges):
        accepted = []
        for rule in workflows[cur_wf_name].rules:
            if len(rule) > 1:
                wf_name = rule[3]
                if rule[1] == "<":
                    ranges_cpy = copy.deepcopy(ranges)
                    if ranges_cpy[rule[0]][0] < rule[2]:
                        ranges_cpy[rule[0]][1] = min(ranges_cpy[rule[0]][1], rule[2] - 1)
                        if rule[3] == "A":
                            accepted.append(ranges_cpy)
                        elif rule[3] != "R":
                            accepted.extend(self.find_ranges(workflows, rule[3], ranges_cpy))
                        ranges[rule[0]][0] = max(ranges[rule[0]][0], rule[2])
                else:
                    ranges_cpy = copy.deepcopy(ranges)
                    if ranges_cpy[rule[0]][1] > rule[2]:
                        ranges_cpy[rule[0]][0] = max(ranges_cpy[rule[0]][0], rule[2] + 1)
                        if rule[3] == "A":
                            accepted.append(ranges_cpy)
                        elif rule[3] != "R":
                            accepted.extend(self.find_ranges(workflows, rule[3], ranges_cpy))
                        ranges[rule[0]][1] = min(ranges[rule[0]][1], rule[2])
            else:
                wf_name = rule[0]
                if wf_name == "A":
                    accepted.append(ranges)
                elif wf_name != "R":
                    accepted.extend(self.find_ranges(workflows, wf_name, ranges))
        return accepted


def part_1(workflows, ratings):
    retval = 0
    for rating in ratings:
        result = rating.calculate(workflows)
        if result == "A":
            retval += rating.values["x"] + rating.values["m"] + rating.values["a"] + rating.values["s"]
    return retval


def part_2(workflows, ratings):
    ranges = {}
    ranges["x"] = [1, 4000]
    ranges["m"] = [1, 4000]
    ranges["a"] = [1, 4000]
    ranges["s"] = [1, 4000]
    accepted_ranges = ratings[0].find_ranges(workflows, "in", ranges)

    retval = 0
    for ranges in accepted_ranges:
        retval += (ranges["x"][1] - ranges["x"][0] + 1) * (ranges["m"][1] - ranges["m"][0] + 1) * (ranges["a"][1] - ranges["a"][0] + 1) * (ranges["s"][1] - ranges["s"][0] + 1)
    return retval


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    workflows = {}
    ratings = []
    decoding_w = True
    for line in lines:
        line = line.strip()
        if decoding_w:
            if len(line) > 0:
                re_line = re.match(r"(.*){(.*)}", line)
                workflows[re_line.group(1)] = Workflow(re_line.group(1), re_line.group(2))
            else:
                decoding_w = False
        else:
            ratings.append(Rating(line))

    if args.part == 1:
        result = part_1(workflows, ratings)
    else:
        result = part_2(workflows, ratings)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
