import argparse
import os

NEIGHBOURS = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
]


def get_neighbours(coords, lands):
    retval = []
    for neighbour in NEIGHBOURS:
        neigh_x = coords[0] + neighbour[0]
        neigh_y = coords[1] + neighbour[1]
        if 0 <= neigh_x < len(lands[0]) and 0 <= neigh_y < len(lands):
            if lands[neigh_y][neigh_x] == ".":
                retval.append((neigh_x, neigh_y))
    return retval


def get_neighbours_part2(coords, lands):
    retval = []
    for neighbour in NEIGHBOURS:
        neigh_x = coords[0] + neighbour[0]
        neigh_y = coords[1] + neighbour[1]
        if lands[neigh_y % len(lands)][neigh_x % len(lands[0])] == ".":
            retval.append((neigh_x, neigh_y))
    return retval


def part_1(lands, start, test):
    neighbours = [start]
    for _ in range(6 if test else 64):
        new_neighbours = []
        for neighbour in neighbours:
            new_neighbours.extend(get_neighbours(neighbour, lands))
        neighbours = list(set(new_neighbours))
    return len(neighbours)


def part_2(lands, start, test):
    neighbours = [start]
    for _ in range(500 if test else 64):
        new_neighbours = []
        for neighbour in neighbours:
            new_neighbours.extend(get_neighbours_part2(neighbour, lands))
        neighbours = list(set(new_neighbours))
    return len(neighbours)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    start = None
    lands = []
    for y, line in enumerate(lines):
        lands.append([])
        for x, char in enumerate(line.strip()):
            if char == "S":
                start = (x, y)
                char = "."
            lands[y].append(char)

    if args.part == 1:
        result = part_1(lands, start, args.test)
    else:
        result = part_2(lands, start, args.test)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
