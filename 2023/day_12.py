import argparse
import os
import functools


@functools.cache
def get_possible_pattern_count(groups, record, first=True):
    if len(groups) == 0:
        # all '.' pattern so lets see there is no '#'
        if "#" not in record:
            return 1
        return 0

    count = 0
    target_spaces = len(record) - sum(groups)
    for space in range(0 if first else 1, target_spaces + 1):
        match = True
        index_cpy = 0
        while space > 0:
            if record[index_cpy] not in ('?', '.'):
                match = False
            index_cpy += 1
            space -= 1
        if match and len(groups) > 0:
            broken = groups[0]
            while broken > 0:
                if record[index_cpy] not in ('?', '#'):
                    match = False
                index_cpy += 1
                broken -= 1
        if match:
            count += get_possible_pattern_count(groups[1:], record[index_cpy:], False)
    return count


def part_1(lines):
    result = 0
    for line in lines:
        line_split = line.split(" ")
        record = line_split[0].strip()
        group_sizes = [int(size) for size in line_split[1].strip().split(",")]
        result += get_possible_pattern_count(group_sizes, record)
    return result


def part_2(lines):
    result = 0
    for line in lines:
        print(line)
        line_split = line.split(" ")
        spring_txt = line_split[0].strip()
        record = "?".join((spring_txt, spring_txt, spring_txt, spring_txt, spring_txt))
        group_sizes = [int(size) for size in line_split[1].strip().split(",")] * 5
        result += get_possible_pattern_count(tuple(group_sizes), record)
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
    else:
        result = part_2(lines)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
