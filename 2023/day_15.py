import argparse
import os
import re


def get_lens_hash(code):
    hashed_code = 0
    for char in code:
        hashed_code += ord(char)
        hashed_code *= 17
        hashed_code %= 256
    return hashed_code


def part_1(codes):
    retval = 0
    for code in codes:
        hashed_code = get_lens_hash(code)
        retval += hashed_code
    return retval


def part_2(codes):
    boxes = [[] for _ in range(256)]
    for code in codes:
        code_matches = re.match(r"(.*)([-=])(.*)", code)
        label = code_matches.group(1)
        box_nr = get_lens_hash(label)
        operation = code_matches.group(2)
        focal_length = code_matches.group(3)
        if operation == "=":
            for lens in boxes[box_nr]:
                if lens[0] == label:
                    lens[1] = int(focal_length)
                    break
            else:
                boxes[box_nr].append([label, int(focal_length)])
        else:
            for lens in boxes[box_nr]:
                if lens[0] == label:
                    boxes[box_nr].remove(lens)
    focus_pwr = 0
    for box_nr, box in enumerate(boxes):
        for lens_nr, lens in enumerate(box):
            focus_pwr += (box_nr + 1) * (lens_nr + 1) * (lens[1])
    return focus_pwr


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    codes = lines[0].strip().split(",")

    if args.part == 1:
        result = part_1(codes)
    else:
        result = part_2(codes)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
