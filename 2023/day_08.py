import argparse
import os
import re
import math


TEST1_P1 = [
    "RL",
    "",
    "AAA = (BBB, CCC)",
    "BBB = (DDD, EEE)",
    "CCC = (ZZZ, GGG)",
    "DDD = (DDD, DDD)",
    "EEE = (EEE, EEE)",
    "GGG = (GGG, GGG)",
    "ZZZ = (ZZZ, ZZZ)",
]


TEST2_P1 = [
    "LLR",
    "",
    "AAA = (BBB, BBB)",
    "BBB = (AAA, ZZZ)",
    "ZZZ = (ZZZ, ZZZ)",
]


class Node:
    def __init__(self, line):
        line_re = re.match(r"(...) = \((...), (...)\)", line)
        self.name = line_re.group(1)
        self.left = line_re.group(2)
        self.right = line_re.group(3)

    def __str__(self):
        return f"Node @ {self.name} [ {self.left} , {self.right} ]"


def find_first_z(part1, start_node, order, nodes):
    searching = True
    steps = 0
    cur_pos = start_node
    while searching:
        for char in order:
            if char == "L":
                cur_pos = nodes[cur_pos].left
            else:
                cur_pos = nodes[cur_pos].right
            steps += 1
            if part1:
                if cur_pos == "ZZZ":
                    searching = False
                    break
            else:
                if cur_pos[2] == "Z":
                    searching = False
                    break
    return steps


def part_1(order, nodes):
    return find_first_z(True, "AAA", order, nodes)


def part_2(order, nodes):
    nr_steps = []
    for name in nodes:
        if "A" in name:
            nr_steps.append(find_first_z(False, name, order, nodes))
    return math.lcm(*nr_steps)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    if args.part == 1 and args.test:
        lines = TEST1_P1
        # lines = TEST2_P1
    else:
        input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
        with open(input_file, "r", encoding="utf8") as infile:
            lines = infile.readlines()

    order = list(lines[0].strip())

    nodes = {}
    for line in lines[2:]:
        new_node = Node(line)
        nodes[new_node.name] = new_node

    if args.part == 1:
        result = part_1(order, nodes)
    else:
        result = part_2(order, nodes)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
