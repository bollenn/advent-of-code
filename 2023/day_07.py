import argparse
import os

CARD_VALUE_P1 = [
    "2", "3", "4", "5",  "6", "7", "8", "9", "T", "J", "Q", "K", "A",
]

CARD_VALUE_P2 = [
    "J", "2", "3", "4", "5",  "6", "7", "8", "9", "T", "Q", "K", "A",
]

TYPE_ORDER = [
    [],
    [2],
    [2, 2],
    [3],
    [3, 2],
    [4],
    [5],
]


class HandOfCards:
    def __init__(self, nr, input_data, part):
        self.nr = nr
        self.cards = list(input_data.split()[0])
        self.bid = int(input_data.split()[1])
        self.part_nr = part
        if self.part_nr == 1:
            self.card_values = CARD_VALUE_P1
        else:
            self.card_values = CARD_VALUE_P2
        self.type_hand = self._calc_type()

    def __str__(self):
        return f"hand {self.nr} : {' '.join(char for char in self.cards)} with bid {self.bid} and type {self.type_hand}"

    def _calc_type(self):
        diff_cards = {}
        for card in self.cards:
            if card in diff_cards:
                diff_cards[card] += 1
            else:
                diff_cards[card] = 1
        if self.part_nr != 1:
            if "J" in diff_cards:
                best = 0
                best_card = ""
                for card, total in diff_cards.items():
                    if card != "J" and total > best:
                        best = total
                        best_card = card
                if best != 0:
                    diff_cards[best_card] += diff_cards["J"]
                    del diff_cards["J"]
        type_calc = [value for value in diff_cards.values() if value > 1]
        type_calc.sort(reverse=True)
        return TYPE_ORDER.index(type_calc)

    def __gt__(self, other):
        if self.type_hand > other.type_hand:
            return True
        if self.type_hand == other.type_hand:
            for nr, card in enumerate(self.cards):
                if self.card_values.index(card) > self.card_values.index(other.cards[nr]):
                    return True
                if self.card_values.index(card) < self.card_values.index(other.cards[nr]):
                    return False
        return False

    def __lt__(self, other):
        if self.type_hand < other.type_hand:
            return True
        if self.type_hand == other.type_hand:
            for nr, card in enumerate(self.cards):
                if self.card_values.index(card) < self.card_values.index(other.cards[nr]):
                    return True
                if self.card_values.index(card) > self.card_values.index(other.cards[nr]):
                    return False
        return False


def part_1(hands):
    result = 0
    hands.sort()
    for nr, hand in enumerate(hands):
        result += (nr + 1) * hand.bid
    return result


def part_2(hands):
    result = 0
    hands.sort()
    for nr, hand in enumerate(hands):
        result += (nr + 1) * hand.bid
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    hands = []
    for nr, line in enumerate(lines):
        hands.append(HandOfCards(nr, line, args.part))

    if args.part == 1:
        result = part_1(hands)
    else:
        result = part_2(hands)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
