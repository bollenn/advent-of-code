import argparse
import os
import itertools


class Galaxy:
    def __init__(self, nr, x, y):
        self.nr = nr
        self.x = x
        self.y = y

    def __str__(self):
        return f"galaxy {self.nr} @ [{self.x}, {self.y}]"

    def measure_dist(self, next_galaxy):
        return abs(self.x - next_galaxy.x) + abs(self.y - next_galaxy.y)

    def expand(self, expand_x, expand_y, size_x, size_y):
        if expand_x is not None and expand_x < self.x:
            self.x += max(size_x - 1, 1)
        if expand_y is not None and expand_y < self.y:
            self.y += max(size_y - 1, 1)


class Image:
    def __init__(self, lines):
        self.galaxies = []
        gal_nr = 1
        self.max_x = 0
        self.max_y = 0
        for gal_y, line in enumerate(lines):
            for gal_x, char in enumerate(line.strip()):
                if char == "#":
                    self.galaxies.append(Galaxy(gal_nr, gal_x, gal_y))
                    gal_nr += 1
                    if gal_x > self.max_x:
                        self.max_x = gal_x
                    if gal_y > self.max_y:
                        self.max_y = gal_y
        self.max_x += 1
        self.max_y += 1

    def __str__(self):
        retval = ""
        for y_pos in range(self.max_y):
            for x_pos in range(self.max_x):
                galaxy = self.get_galaxy(x_pos, y_pos)
                if galaxy is not None:
                    retval += f"{galaxy.nr}"
                else:
                    retval += "."
            retval += "\n"
        return retval

    def get_galaxy(self, xpos, ypos):
        for galaxy in self.galaxies:
            if galaxy.x == xpos and galaxy.y == ypos:
                return galaxy
        return None

    def expand(self, x_offset, y_offset):
        expand_x = [True] * self.max_x
        expand_y = [True] * self.max_y
        for galaxy in self.galaxies:
            expand_x[galaxy.x] = False
            expand_y[galaxy.y] = False
        for ex_x, expand in reversed(list(enumerate(expand_x))):
            if expand:
                for galaxy in self.galaxies:
                    galaxy.expand(ex_x, None, x_offset, y_offset)
        for ex_y, expand in reversed(list(enumerate(expand_y))):
            if expand:
                for galaxy in self.galaxies:
                    galaxy.expand(None, ex_y, x_offset, y_offset)
        self.max_x += expand_x.count(True) * (max(x_offset - 1, 1))
        self.max_y += expand_y.count(True) * (max(y_offset - 1, 1))


def part_1(image):
    result = 0
    image.expand(1, 1)
    galaxy_combos = list(itertools.combinations(image.galaxies, 2))
    for combo in galaxy_combos:
        if combo[0] != combo[1]:
            distance = combo[0].measure_dist(combo[1])
            result += distance
    return result


def part_2(image):
    result = 0
    image.expand(1000000, 1000000)
    galaxy_combos = list(itertools.combinations(image.galaxies, 2))
    for combo in galaxy_combos:
        if combo[0] != combo[1]:
            distance = combo[0].measure_dist(combo[1])
            result += distance
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    image = Image(lines)

    if args.part == 1:
        result = part_1(image)
    else:
        result = part_2(image)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
