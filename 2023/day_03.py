import argparse
import os


class Number:
    def __init__(self, parent, start_x, start_y, end_x, end_y, value):
        self.parent = parent
        self.start_x = start_x
        self.start_y = start_y
        self.end_x = end_x
        self.end_y = end_y
        self.value = value

    def __str__(self):
        return f"{self.value} @ [{self.start_x}, {self.start_y}] [{self.end_x}, {self.end_y}]"

    def check_for_symbol(self):
        for ypos in range(self.start_y-1, self.end_y+2):
            for xpos in range(self.start_x-1, self.end_x+2):
                neighbour = self.parent.get(xpos, ypos)
                if not neighbour.isnumeric() and neighbour != ".":
                    return True
        return False

    def in_range(self, x, y):
        return self.start_x <= x <= self.end_x and self.start_y <= y <= self.end_y


class Star:
    def __init__(self, parent, x, y):
        self.parent = parent
        self.x = x
        self.y = y

    def __str__(self):
        return f"Star @ [{self.x}, {self.y}]"

    def gear_nrs(self, numbers):
        touch_nr = []
        for ypos in range(self.y-1, self.y+2):
            for xpos in range(self.x-1, self.x+2):
                for number in numbers:
                    if number.in_range(xpos, ypos) and number not in touch_nr:
                        touch_nr.append(number)
        return touch_nr


class Schematic:
    def __init__(self, lines):
        self._schematic = []
        for y, line in enumerate(lines):
            line_chars = []
            for x, char in enumerate(line.strip()):
                line_chars.append(char)
            self._schematic.append(line_chars)

    def __str__(self):
        retval = ""
        for line in self._schematic:
            retval += "".join(char for char in line)
        return retval

    def get(self, x, y):
        if 0 <= x < len(self._schematic[0]) and 0 <= y < len(self._schematic):
            return self._schematic[y][x]
        return "."

    def get_numbers(self):
        numbers = []
        for y in range(len(self._schematic)):
            cur_number = None
            start_x = 0
            for x in range(len(self._schematic[0])):
                char = self.get(x, y)
                if char.isnumeric():
                    if cur_number is None:
                        start_x = x
                        cur_number = char
                    else:
                        cur_number += char
                else:
                    if cur_number is not None:
                        numbers.append(Number(self, start_x, y, x - 1, y, int(cur_number)))
                    cur_number = None
            if cur_number is not None:
                numbers.append(Number(self, start_x, y, x - 1, y, int(cur_number)))
        return numbers

    def get_stars(self):
        stars = []
        for y in range(len(self._schematic)):
            for x in range(len(self._schematic[0])):
                if self.get(x, y) == "*":
                    stars.append(Star(self, x, y))
        return stars


def part_1(schematic):
    result = 0
    for number in schematic.get_numbers():
        if number.check_for_symbol():
            result += number.value
    return result


def part_2(schematic):
    result = 0
    numbers = schematic.get_numbers()
    for star in schematic.get_stars():
        touch_nr = star.gear_nrs(numbers)
        if len(touch_nr) == 2:
            result += touch_nr[0].value * touch_nr[1].value
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    schematic = Schematic(lines)

    if args.part == 1:
        result = part_1(schematic)
    else:
        result = part_2(schematic)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
