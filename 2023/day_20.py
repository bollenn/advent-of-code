import argparse
import os
import re
import math

LOW_PULSE = False
HIGH_PULSE = True

TEST_PART1_A = [
    "broadcaster -> a, b, c",
    "%a -> b",
    "%b -> c",
    "%c -> inv",
    "&inv -> a",
]

TEST_PART1_B = [
    "broadcaster -> a",
    "%a -> inv, con",
    "&inv -> b",
    "%b -> con",
    "&con -> output",
]


class BaseLogicBlock:
    def __init__(self, name):
        self.name = name
        self.outputs = []
        self.inputs = []
        self.type_descr = ""

    def __str__(self):
        return f"{self.type_descr} logic '{self.name}''"

    def sync_im_your_input(self, input_block_name):
        if input_block_name not in self.inputs:
            self.inputs.append(input_block_name)

    def do_logic(self, signal_from, signal_lvl):
        print(f"{signal_from} -> {signal_lvl} -> {self.name} ")
        return {self.name: {}}


class OutputLogicBlock(BaseLogicBlock):
    def __init__(self, name):
        super().__init__(name)
        self.type_descr = "output"


class BroadcastLogicBlock(BaseLogicBlock):
    def __init__(self, name, outputs):
        super().__init__(name)
        self.outputs = outputs
        self.type_descr = "broadcast"

    def do_logic(self, signal_from, signal_lvl):
        print(f"{signal_from} -> {signal_lvl} -> {self.name}")
        return {self.name: {output: signal_lvl for output in self.outputs}}


class FlipFlopLogicBlock(BaseLogicBlock):
    def __init__(self, name, outputs):
        super().__init__(name)
        self.outputs = outputs
        self.type_descr = "flipflop"
        self.status = LOW_PULSE

    def do_logic(self, signal_from, signal_lvl):
        print(f"{signal_from} -> {signal_lvl} -> {self.name}")
        if signal_lvl == LOW_PULSE:
            self.status = not self.status
            return {self.name: {output: self.status for output in self.outputs}}
        return {self.name: {}}


class ConjunctionLogicBlock(BaseLogicBlock):
    def __init__(self, name, outputs):
        super().__init__(name)
        self.outputs = outputs
        self.changed = False
        self.type_descr = "conjunction"
        self.pulses_memory = {}

    def do_logic(self, signal_from, signal_lvl):
        print(f"{signal_from} -> {signal_lvl} -> {self.name} ")
        if len(self.pulses_memory) != len(self.inputs):
            for input_block in self.inputs:
                if input_block not in self.pulses_memory:
                    self.pulses_memory[input_block] = LOW_PULSE
        self.pulses_memory[signal_from] = signal_lvl
        if all(self.pulses_memory.values()):
            return {self.name: {output: LOW_PULSE for output in self.outputs}}
        return {self.name: {output: HIGH_PULSE for output in self.outputs}}


def part_1(blocks):
    count = 1000
    all_high_pulses = 0
    all_low_pulses = 0
    while count > 0:
        all_low_pulses += 1  # add one for button signal
        blocks_to_update = [blocks["broadcaster"].do_logic("button", LOW_PULSE)]

        while len(blocks_to_update) > 0:
            new_blocks_to_update = []
            for updates in blocks_to_update:
                for from_block, signals_to_send in updates.items():
                    for to_block, signal_lvl in signals_to_send.items():
                        if signal_lvl:
                            all_high_pulses += 1
                        else:
                            all_low_pulses += 1
                        new_blocks_to_update.append(blocks[to_block].do_logic(from_block, signal_lvl))
            blocks_to_update = new_blocks_to_update
        count -= 1
    return all_high_pulses * all_low_pulses


def part_2(blocks):
    # record first low signals button pushes for input to 'rx' output
    rx_inputs = blocks["rm"].inputs
    rx_input_toggles = [None] * len(rx_inputs)

    count = 0
    while True:
        count += 1
        blocks_to_update = [blocks["broadcaster"].do_logic("button", LOW_PULSE)]

        while len(blocks_to_update) > 0:
            new_blocks_to_update = []
            for updates in blocks_to_update:
                for from_block, signals_to_send in updates.items():
                    for to_block, signal_lvl in signals_to_send.items():
                        new_blocks_to_update.append(blocks[to_block].do_logic(from_block, signal_lvl))
                        if to_block in rx_inputs and not signal_lvl:
                            if rx_input_toggles[rx_inputs.index(to_block)] is None:
                                rx_input_toggles[rx_inputs.index(to_block)] = count
            blocks_to_update = new_blocks_to_update
        if all(in_block is not None for in_block in rx_input_toggles):
            break
    return math.lcm(*rx_input_toggles)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    if args.test:
        # lines = TEST_PART1_A
        lines = TEST_PART1_B
    else:
        input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
        with open(input_file, "r", encoding="utf8") as infile:
            lines = infile.readlines()

    blocks = {}
    for line in lines:
        re_line = re.match(r"([%|&]?)(.*) -> (.*)", line.strip())
        name = re_line.group(2)
        logic_type = re_line.group(1)
        outputs = [output.strip() for output in re_line.group(3).split(",")]
        if logic_type == "%":
            logic_block = FlipFlopLogicBlock(name, outputs)
        elif logic_type == "&":
            logic_block = ConjunctionLogicBlock(name, outputs)
        else:
            logic_block = BroadcastLogicBlock(name, outputs)
        blocks[name] = logic_block

    extra_outputs = {}
    for block in blocks.values():
        for output in block.outputs:
            if output not in blocks:
                extra_outputs[output] = OutputLogicBlock(output)
                extra_outputs[output].sync_im_your_input(block.name)
            else:
                blocks[output].sync_im_your_input(block.name)
    blocks.update(extra_outputs)

    if args.part == 1:
        result = part_1(blocks)
    else:
        result = part_2(blocks)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
