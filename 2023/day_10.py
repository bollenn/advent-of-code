import argparse
import os
import matplotlib.pyplot as plt

FROM_LEFT = 0
FROM_TOP = 1
FROM_RIGHT = 2
FROM_BOTTOM = 3

# directions to go in [x, y] [From Left, From Top, From Right, From Bottom]
DIRS = {
    "F": [None, None, [0,-1], [1,0]],
    "L": [None, [1,0], [0,1], None],
    "7": [[0,-1],None,None,[-1, 0]],
    "J": [[0,1], [-1,0], None, None],
    "|": [None, [0,-1], None, [0,1]],
    "-": [[1,0], None, [-1,0], None],
    "S": [None, None, None, None],
}

P1_TEST1 = [
    "-L|F7",
    "7S-7|",
    "L|7||",
    "-L-J|",
    "L|-JF",
]


P1_TEST2 = [
    "7-F7-",
    ".FJ|7",
    "SJLL7",
    "|F--J",
    "LJ.LJ",
]

P2_TEST = [
    ".F----7F7F7F7F-7....",
    ".|F--7||||||||FJ....",
    ".||.FJ||||||||L7....",
    "FJL7L7LJLJ||LJ.L-7..",
    "L--J.L7...LJS7F-7L7.",
    "....F-J..F7FJ|L7L7L7",
    "....L7.F7||L7|.L7L7|",
    ".....|FJLJ|FJ|F7|.LJ",
    "....FJL-7.||.||||...",
    "....L---J.LJ.LJLJ...",
]

NEIGHBOURS = [[-1,0], [1,0], [0,1], [0,-1]]


class Land:
    def __init__(self, char, x, y):
        self.char = char
        self.x = x
        self.y = y

    def __str__(self):
        return f"{self.char} @ [{self.x},{self.y}]"

    def next(self, prev_land, lands):
        if self.char == "S":
            return lands[self.y][self.x+1]  # take the lucky shot and assume we can go right
        from_pos = None
        if prev_land.x == self.x:
            if prev_land.y > self.y:
                from_pos = FROM_BOTTOM
            else:
                from_pos = FROM_TOP
        else:
            if prev_land.x > self.x:
                from_pos = FROM_RIGHT
            else:
                from_pos = FROM_LEFT
        return lands[self.y - DIRS[self.char][from_pos][1]][self.x + DIRS[self.char][from_pos][0]]


class Drawing:
    def __init__(self, dim_x, dim_y):
        self.drawing = []
        for line_nr in range(dim_y*2):
            self.drawing.append([])
            for pos_nr in range(dim_x*2):
                if line_nr % 2 != 0 or pos_nr % 2 != 0:
                    self.drawing[line_nr].append(1)
                else:
                    self.drawing[line_nr].append(5)

    def __str__(self):
        retval = ""
        for line in self.drawing:
            retval += "".join(f"{char}" for char in line)
            retval += "\n"
        return retval

    def _get_land_value(self, x, y):
        if 0 <= x < len(self.drawing[0]) and 0 <= y < len(self.drawing):
            return self.drawing[y][x]
        return 0

    def _set_land_value(self, x, y, value):
        if 0 <= x < len(self.drawing[0]) and 0 <= y < len(self.drawing):
            self.drawing[y][x] = value

    def insert_move(self, from_land, to_land):
        self.drawing[from_land.y*2][from_land.x*2] = 9
        self.drawing[(from_land.y*2)+(to_land.y-from_land.y)][(from_land.x*2)+(to_land.x-from_land.x)] = 9

    def delete_outer(self):
        deleted = True
        while deleted:
            deleted = False
            for y in range(len(self.drawing)):
                for x in range(len(self.drawing[0])):
                    if self._get_land_value(x, y) != 9 and self._get_land_value(x, y) != 0:
                        if (self._get_land_value(x, y-1) == 0 or self._get_land_value(x, y+1) == 0 or
                            self._get_land_value(x-1, y) == 0 or self._get_land_value(x+1, y) == 0):
                            self._set_land_value(x, y, 0)
                            deleted = True

    def count_inner(self):
        # count 5's
        result = 0
        for y, line in enumerate(self.drawing):
            for x, char in enumerate(line):
                if char == 5:
                    result += 1
        return result

    def plot(self):
        im = plt.imshow(self.drawing, cmap='Greys')
        plt.show()


def part_1(lands):
    # find start
    start = None
    for landrow in lands:
        for land in landrow:
            if land.char == "S":
                start = land
                break

    # create drawing
    drawing = Drawing(len(lands[0]), len(lands))

    # find and draw loop
    count = 1
    prev = start
    next = start.next(lands[start.y][start.x-1], lands)
    drawing.insert_move(start, next)
    while next.char != "S":
        new = next.next(prev, lands)
        drawing.insert_move(next, new)
        prev = next
        next = new
        count += 1

    return int(count / 2), drawing


def part_2(lands):
    _, drawing = part_1(lands)

    drawing.delete_outer()

    drawing.plot()

    return drawing.count_inner()


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    if args.test:
        lines = P2_TEST
    else:
        input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
        with open(input_file, "r", encoding="utf8") as infile:
            lines = infile.readlines()

    lands = []
    for y, line in enumerate(lines):
        lands.append([Land(char, x, y) for x, char in enumerate(line.strip())])

    if args.part == 1:
        result, _ = part_1(lands)
    else:
        result = part_2(lands)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
