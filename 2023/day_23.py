import argparse
import os
import copy
import sys

sys.setrecursionlimit(15000)

NEIGHBOURS = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
]


class Map:
    def __init__(self, lines):
        self.lands = []
        for line in lines:
            land_line = []
            for char in line.strip():
                land_line.append(char)
            self.lands.append(land_line)
        self.start = self.get_start_coord()
        self.end = self.get_end_coord()

    def __str__(self):
        retval = ""
        for line in self.lands:
            retval += "".join(char for char in line)
            retval += "\n"
        return retval

    def get_start_coord(self):
        x = None
        for x, char in enumerate(self.lands[0]):
            if char == ".":
                break
        return [x, 0]

    def get_end_coord(self):
        x = None
        for x, char in enumerate(self.lands[-1]):
            if char == ".":
                break
        return [x, len(self.lands) - 1]

    def get_land(self, x, y):
        if 0 <= x < len(self.lands[0]) and 0 <= y < len(self.lands):
            return self.lands[y][x]
        return "#"


def move_multiple_lands(land_map, route, x, y, direction, part2):
    new_x = x
    new_y = y
    copied = False
    new_route = None
    while True:
        if [new_x, new_y] in route:
            break
        neighbour_land = land_map.get_land(new_x, new_y)
        if neighbour_land == "#":
            break
        if (not part2 and ((neighbour_land == "<" and direction != [-1, 0]) or
             (neighbour_land == ">" and direction != [1, 0]) or
             (neighbour_land == "v" and direction != [0, 1]) or
             (neighbour_land == "^" and direction != [0, -1]))):
            break
        if new_route is None:
            new_route = copy.copy(route)
        new_route.append([new_x, new_y])
        if direction[0] != 0:
            # moving x direction
            if land_map.get_land(new_x, new_y-1) != "#" or land_map.get_land(new_x, new_y+1) != "#":
                break
        else:
            # moving y direction
            if land_map.get_land(new_x-1, new_y) != "#" or land_map.get_land(new_x+1, new_y) != "#":
                break
        new_x += direction[0]
        new_y += direction[1]
    return new_route


def do_next_moves(land_map, cur_x, cur_y, route, finisher_lengths, part2):
    for neighbour in NEIGHBOURS:
        neigh_x = cur_x + neighbour[0]
        neigh_y = cur_y + neighbour[1]
        if [neigh_x, neigh_y] in route:
            continue
        new_route = move_multiple_lands(land_map, route, neigh_x, neigh_y, neighbour, part2)
        if new_route is not None:
            if new_route[-1] == land_map.end:
                finisher_lengths.append(len(new_route))
            do_next_moves(land_map, new_route[-1][0], new_route[-1][1], new_route, finisher_lengths, part2)


def part_1(land_map):
    finisher_lengths = []
    do_next_moves(land_map, land_map.start[0], land_map.start[1], [], finisher_lengths, False)
    return max(finisher_lengths)


def part_2(land_map):
    finisher_lengths = []
    do_next_moves(land_map, land_map.start[0], land_map.start[1], [], finisher_lengths, True)
    return max(finisher_lengths)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    land_map = Map(lines)

    if args.part == 1:
        result = part_1(land_map)
    else:
        result = part_2(land_map)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
