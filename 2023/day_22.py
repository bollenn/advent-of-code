import argparse
import os
import re


class Coordinate:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f"Coordinate(x={self.x}, y={self.y}, z={self.z})"


class Brick:
    def __init__(self, line):
        re_line = re.match(r"(\d*),(\d*),(\d*)~(\d*),(\d*),(\d*)", line)
        if int(re_line.group(3)) <= int(re_line.group(6)):
            self.first_coord = Coordinate(int(re_line.group(1)), int(re_line.group(2)), int(re_line.group(3)))
            self.second_coord = Coordinate(int(re_line.group(4)), int(re_line.group(5)), int(re_line.group(6)))
        else:
            self.second_coord = Coordinate(int(re_line.group(1)), int(re_line.group(2)), int(re_line.group(3)))
            self.first_coord = Coordinate(int(re_line.group(4)), int(re_line.group(5)), int(re_line.group(6)))
        self.xy_locs = []
        for x in range(self.first_coord.x, self.second_coord.x + 1):
            for y in range(self.first_coord.y, self.second_coord.y + 1):
                self.xy_locs.append((x, y))
        self.my_supports = []
        self.i_support = []
        self.damaged_supports = []
        self.damaged = False

    def __str__(self):
        return f"{self.first_coord} -> {self.second_coord} -> {self.xy_locs}"

    def __lt__(self, other):
        return self.first_coord.z < other.first_coord.z

    def fall(self, bricks):
        # check for highest brick below this one
        max_z = 0
        for brick in bricks:
            for loc in self.xy_locs:
                if loc in brick.xy_locs:
                    if brick.second_coord.z < self.first_coord.z:
                        max_z = max(brick.second_coord.z, max_z)
                        break
        if max_z + 1 != self.first_coord.z:
            move_z = self.first_coord.z - (max_z + 1)
            self.first_coord.z -= move_z
            self.second_coord.z -= move_z
            return True
        return False

    def update_supports(self, bricks):
        self.my_supports = []
        for brick in bricks:
            for loc in self.xy_locs:
                if loc in brick.xy_locs:
                    if min(self.first_coord.z, self.second_coord.z) == max(brick.first_coord.z, brick.second_coord.z) + 1:
                        self.my_supports.append(brick)
                        break
        self.i_support = []
        for brick in bricks:
            for loc in self.xy_locs:
                if loc in brick.xy_locs:
                    if max(self.first_coord.z, self.second_coord.z) == min(brick.first_coord.z, brick.second_coord.z) - 1:
                        self.i_support.append(brick)
                        break

    def my_support_bricks(self):
        """ list of Bricks: bricks supporting this brick """
        return self.my_supports

    def bricks_i_support(self):
        """ list of Bricks: bricks this brick is supporting """
        return self.i_support

    def set_damaged(self):
        self.damaged = True
        if len(self.i_support) != 0:
            for supported in self.i_support:
                supported.damage_support(self)

    def damage_support(self, damaged_brick):
        if damaged_brick in self.my_supports and damaged_brick not in self.damaged_supports:
            self.damaged_supports.append(damaged_brick)
        if len(self.my_supports) > 0 and len(self.damaged_supports) == len(self.my_supports):
            self.set_damaged()

    def reset_damage(self):
        self.damaged_supports = []
        self.damaged = False


def let_all_bricks_fall_to_botom(bricks):
    bricks.sort()
    while True:
        moved = False
        for brick in bricks:
            moved |= brick.fall(bricks)
        if not moved:
            break
    for brick in bricks:
        brick.update_supports(bricks)


def part_1(bricks):
    let_all_bricks_fall_to_botom(bricks)
    safe = 0
    for brick in bricks:
        above_bricks = brick.bricks_i_support()
        if len(above_bricks) == 0:
            safe += 1
            continue
        for above_brick in above_bricks:
            above_supports = above_brick.my_support_bricks()
            if len(above_supports) <= 1:
                break
        else:
            safe += 1
    return safe


def part_2(bricks):
    let_all_bricks_fall_to_botom(bricks)

    result = 0
    for brick in bricks:
        brick.set_damaged()
        damage = 0
        for dam_brick in bricks:
            damage += 1 if dam_brick.damaged else 0
        result += damage - 1
        for resetbrick in bricks:
            resetbrick.reset_damage()

    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    bricks = []
    for line in lines:
        bricks.append(Brick(line))

    if args.part == 1:
        result = part_1(bricks)
    else:
        result = part_2(bricks)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
