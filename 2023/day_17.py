import argparse
import os
import heapq

LEFT = (-1, 0)
DOWN = (0, 1)
RIGHT = (1, 0)
UP = (0, -1)

NEIGHBOURS = (LEFT, DOWN, RIGHT, UP)


def route(city_map, min_steps=1, max_steps=3):
    start_city = (0, 0)
    end_city = (len(city_map[0]) - 1, len(city_map) - 1)
    work = []
    work.append((0, start_city, RIGHT, 1))
    work.append((0, start_city, DOWN, 1))
    visited = set()

    while len(work) > 0:
        cost, curr_city, direction, steps = heapq.heappop(work)
        if (curr_city, direction, steps) in visited:
            continue
        visited.add((curr_city, direction, steps))

        next_x = curr_city[0] + direction[0]
        next_y = curr_city[1] + direction[1]
        if not (0 <= next_x < len(city_map[0]) and 0 <= next_y < len(city_map)):
            continue

        new_cost = cost + city_map[next_y][next_x]

        if min_steps <= steps <= max_steps and next_x == end_city[0] and next_y == end_city[1]:
            return new_cost

        for next_dir in NEIGHBOURS:
            # do not allow reverse movement
            if next_dir[0] == -direction[0] and next_dir[1] == -direction[1]:
                continue

            # check for max steps
            new_steps = steps + 1 if direction == next_dir else 1
            if (steps < min_steps and direction != next_dir) or new_steps > max_steps:
                continue

            heapq.heappush(work, (new_cost, (next_x, next_y), next_dir, new_steps))
    return None


def part_1(city_map):
    return route(city_map)


def part_2(city_map):
    return route(city_map, 4, 10)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    city_map = []
    for line in lines:
        city_map.append([int(char) for char in line.strip()])

    if args.part == 1:
        result = part_1(city_map)
    else:
        result = part_2(city_map)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
