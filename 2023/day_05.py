import argparse
import os
import re
from collections import namedtuple

Range = namedtuple("Range", ["min", "max"])


class MapItem:
    def __init__(self, indata):
        indata = indata.split()
        self.min = int(indata[1])
        self.max = self.min + int(indata[2])
        self.diff = int(indata[0]) - self.min

    def __str__(self):
        return f"map [{self.min}:{self.max}] offset {self.diff}"

    def convert(self, position):
        if not self.min <= position < self.max:
            return None
        return position + self.diff

    def convert_ranges(self, range_objs):
        not_converted = []
        converted = []
        for range_obj in range_objs:
            if range_obj.min < self.min < range_obj.max:
                not_converted.append(Range(range_obj.min, self.min - 1))
            if range_obj.min < self.max < range_obj.max:
                not_converted.append(Range(self.max + 1, range_obj.max))
            if range_obj.max < self.min or range_obj.min > self.max:
                not_converted.append(range_obj)
            else:
                converted.append(Range(max(self.min, range_obj.min) + self.diff, min(range_obj.max, self.max) + self.diff))
        return converted, not_converted


class Maps:
    def __init__(self, maps):
        self.my_maps = maps
        self.next_maps = None

    def set_next_maps(self, next_maps):
        self.next_maps = next_maps

    def convert(self, number):
        for map_item in self.my_maps:
            converted_nr = map_item.convert(number)
            if converted_nr is not None:
                number = converted_nr
                break
        return number

    def get_next_stage_nr(self, number):
        number = self.convert(number)
        if self.next_maps is None:
            return number
        return self.next_maps.get_next_stage_nr(number)

    def convert_ranges(self, range_objs):
        converted = []
        not_conv = range_objs
        for map_item in self.my_maps:
            conv, not_conv = map_item.convert_ranges(not_conv)
            converted.extend(conv)
        converted.extend(not_conv)
        if self.next_maps is not None:
            return self.next_maps.convert_ranges(converted)
        return min(start for start, _ in converted)


def get_maps_items(indata):
    items = []
    for line in indata.split("\n"):
        if line.strip():
            items.append(MapItem(line))
    return Maps(items)


def part_1(seed_data, maps):
    locations = []
    for seed in seed_data:
        locations.append(maps[0].convert_ranges([Range(seed, seed)]))
    return min(locations)


def part_2(seed_data, maps):
    locations = []
    for i in range(0, len(seed_data), 2):
        locations.append(maps[0].convert_ranges([Range(seed_data[i], seed_data[i] + seed_data[i+1])]))
    return min(locations)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    text = "".join(line for line in lines)

    seed_data = [int(nr) for nr in re.findall("seeds:\s(.*)", text)[0].split()]

    seed_to_soil_map = get_maps_items(re.search("seed-to-soil map:\n((?:(\d*\s?)\n?)+)", text).group(1))
    soil_to_fertilizer_map = get_maps_items(re.search("soil-to-fertilizer map:\n((?:(\d*\s?)\n?)+)", text).group(1))
    fertilizer_to_water_map = get_maps_items(re.search("fertilizer-to-water map:\n((?:(\d*\s?)\n?)+)", text).group(1))
    water_to_light_map = get_maps_items(re.search("water-to-light map:\n((?:(\d*\s?)\n?)+)", text).group(1))
    light_to_temperature_map = get_maps_items(re.search("light-to-temperature map:\n((?:(\d*\s?)\n?)+)", text).group(1))
    temperature_to_humidity_map = get_maps_items(re.search("temperature-to-humidity map:\n((?:(\d*\s?)\n?)+)", text).group(1))
    humidity_to_location_map = get_maps_items(re.search("humidity-to-location map:\n((?:(\d*\s?)\n?)+)", text).group(1))

    seed_to_soil_map.set_next_maps(soil_to_fertilizer_map)
    soil_to_fertilizer_map.set_next_maps(fertilizer_to_water_map)
    fertilizer_to_water_map.set_next_maps(water_to_light_map)
    water_to_light_map.set_next_maps(light_to_temperature_map)
    light_to_temperature_map.set_next_maps(temperature_to_humidity_map)
    temperature_to_humidity_map.set_next_maps(humidity_to_location_map)
    humidity_to_location_map.set_next_maps(None)

    maps = []
    maps.append(seed_to_soil_map)
    maps.append(soil_to_fertilizer_map)
    maps.append(fertilizer_to_water_map)
    maps.append(water_to_light_map)
    maps.append(light_to_temperature_map)
    maps.append(temperature_to_humidity_map)
    maps.append(humidity_to_location_map)

    if args.part == 1:
        result = part_1(seed_data, maps)
    else:
        result = part_2(seed_data, maps)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
