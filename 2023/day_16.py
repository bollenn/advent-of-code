import argparse
import os


MOVES = {
    "LR": [1, 0],
    "TB": [0, 1],
    "RL": [-1, 0],
    "BT": [0, -1],
}


class Tile:
    def __init__(self, char):
        self.char = char
        self.lit = 0
        self.passes = {
            "LR": 0,
            "TB": 0,
            "RL": 0,
            "BT": 0,
        }

    def __str__(self):
        return self.char

    def convert_beam(self, beam):
        return [beam]

    def reset(self):
        self.lit = 0
        self.passes = {
            "LR": 0,
            "TB": 0,
            "RL": 0,
            "BT": 0,
        }


class MirrorTile(Tile):
    def convert_beam(self, beam):
        if self.char == "/":
            conversion = {
                "LR": "BT",
                "TB": "RL",
                "RL": "TB",
                "BT": "LR",
            }
            beam.dir = conversion[beam.dir]
            return [beam]
        conversion = {
            "LR": "TB",
            "TB": "LR",
            "RL": "BT",
            "BT": "RL",
        }
        beam.dir = conversion[beam.dir]
        return [beam]


class SplitterTile(Tile):
    def convert_beam(self, beam):
        if self.char == "|":
            if beam.dir in ("LR", "RL"):
                # split
                beam1 = Beam(beam.x, beam.y, "TB")
                beam2 = Beam(beam.x, beam.y, "BT")
                return [beam1, beam2]
            return [beam]
        if beam.dir in ("TB", "BT"):
            # split
            beam1 = Beam(beam.x, beam.y, "LR")
            beam2 = Beam(beam.x, beam.y, "RL")
            return [beam1, beam2]
        return [beam]


class EmptyTile(Tile):
    pass


TILES = {
    "/": MirrorTile,
    "\\": MirrorTile,
    "|": SplitterTile,
    "-": SplitterTile,
    ".": EmptyTile,
}


class Contraption:
    def __init__(self, lines):
        self.grid = []
        for line in lines:
            tiles = []
            for tile in line.strip():
                tiles.append(TILES[tile](tile))
            self.grid.append(tiles)

    def __str__(self):
        retval = ""
        for gridline in self.grid:
            retval += "".join(f"{tile.lit} " for tile in gridline)
            retval += "\n"
        return retval

    def get(self, x, y):
        if 0 <= x < len(self.grid[0]) and 0 <= y < len(self.grid):
            return self.grid[y][x]
        return None

    def count(self):
        retval = 0
        for gridline in self.grid:
            retval += sum(1 if tile.lit > 0 else 0 for tile in gridline)
        return retval

    def reset(self):
        for gridline in self.grid:
            for tile in gridline:
                tile.reset()


class Beam:
    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.dir = direction

    def __str__(self):
        return f"Beam @ [{self.x},{self.y}] traveling {self.dir}"

    def move(self, grid):
        self.x += MOVES[self.dir][0]
        self.y += MOVES[self.dir][1]
        tile = grid.get(self.x, self.y)
        if tile is None:
            # this beam is done moving
            return []
        tile.lit += 1
        if tile.passes[self.dir] >= 1:
            return []
        tile.passes[self.dir] += 1
        return tile.convert_beam(self)


def part_1(contraption, start_x=-1, start_y=0, start_dir="LR"):
    beams = [Beam(start_x, start_y, start_dir)]
    while True:
        next_beams = []
        for beam in beams:
            next_beams.extend(beam.move(contraption))
        if len(next_beams) == 0:
            break
        beams = next_beams
    return contraption.count()


def part_2(contraption):
    totals = []
    for x in range(0, len(contraption.grid[0])):
        totals.append(part_1(contraption, x, -1, "TB"))
        contraption.reset()
        totals.append(part_1(contraption, x, len(contraption.grid), "BT"))
        contraption.reset()
    for y in range(0, len(contraption.grid)):
        totals.append(part_1(contraption, -1, y, "LR"))
        contraption.reset()
        totals.append(part_1(contraption, len(contraption.grid[0]), y, "RL"))
        contraption.reset()
    return max(totals)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    contraption = Contraption(lines)

    if args.part == 1:
        result = part_1(contraption)
    else:
        result = part_2(contraption)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
