import argparse
import os
import re

TEST_P1_INPUT = [
    "1abc2",
    "pqr3stu8vwx",
    "a1b2c3d4e5f",
    "treb7uchet",
]

STR_TO_NR = [
    None,
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine"
]


def part_1(lines):
    cal_vals = []
    for line in lines:
        number_re = re.findall("\d", line.strip())
        if number_re is not None:
            cal_vals.append(int(number_re[0]) * 10 + int(number_re[-1]))
    return sum(cal_vals)


def part_2(lines):
    cal_vals = []
    for line_nr, line in enumerate(lines):
        line_nrs = []
        for char_pos, char in enumerate(line):
            try:
                line_nrs.append(int(char))
            except ValueError:
                # must be a char
                for pos, conv_nr in enumerate(STR_TO_NR):
                    if conv_nr is not None:
                        try:
                            if line.index(conv_nr, char_pos) == char_pos:
                                line_nrs.append(pos)
                        except ValueError:
                            pass
        cal_vals.append(line_nrs[0] * 10 + line_nrs[-1])
    return sum(cal_vals)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    if args.part == 1 and args.test:
        lines = TEST_P1_INPUT
    else:
        input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
        with open(input_file, "r", encoding="utf8") as infile:
            lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
