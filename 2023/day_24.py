import argparse
import os
import re


class Hailstone:
    def __init__(self, line):
        re_line = re.match(r"(\d*),\s*(\d*),\s*(\d*)\s*@\s*([-]?\d*),\s*([-]?\d*),\s*([-]?\d*)", line)
        self.px = int(re_line.group(1))
        self.py = int(re_line.group(2))
        self.pz = int(re_line.group(3))
        self.vx = int(re_line.group(4))
        self.vy = int(re_line.group(5))
        self.vz = int(re_line.group(6))

    def __str__(self):
        return f"[{self.px},{self.py},{self.pz}] @ speed [{self.vx},{self.vy},{self.vz}]"

    def get_y(self, x):
        return (x * (self.vy / self.vx)) - (self.px * (self.vy / self.vx)) + self.py

    def get_collision_point(self, other):
        """ solve linear formula to find collission point
        ((x - self.px) * (self.vy / self.vx)) + self.py = ((x - other.px) * (other.vy / other.vx)) + other.py
        (x * (self.vy / self.vx)) - (self.px * (self.vy / self.vx)) + self.py = (x * (other.vy / other.vx)) - (other.px * (other.vy / other.vx)) + other.py
        (x * (self.vy / self.vx)) - (x * (other.vy / other.vx)) = (self.px * (self.vy / self.vx)) - (other.px * (other.vy / other.vx)) + other.py - self.py
        x * ((self.vy / self.vx) - (other.vy / other.vx)) = (self.px * (self.vy / self.vx)) - (other.px * (other.vy / other.vx)) + other.py - self.py
        x = ((self.px * (self.vy / self.vx)) - (other.px * (other.vy / other.vx)) + other.py - self.py) / ((self.vy / self.vx) - (other.vy / other.vx))
        """
        if ((self.vy / self.vx) - (other.vy / other.vx)) == 0:
            return None
        return ((self.px * (self.vy / self.vx)) - (other.px * (other.vy / other.vx)) + other.py - self.py) / ((self.vy / self.vx) - (other.vy / other.vx))


def part_1(hailstones, test):
    if test:
        min_pos = 7
        max_pos = 27
    else:
        min_pos = 200000000000000
        max_pos = 400000000000000
    count = 0
    for cur_pos, hailstone_a in enumerate(hailstones):
        for hailstone_b in hailstones[cur_pos+1:]:
            if hailstone_a != hailstone_b:
                x = hailstone_a.get_collision_point(hailstone_b)
                if x is not None:
                    y = hailstone_a.get_y(x)
                    if (x - hailstone_a.px) / hailstone_a.vx >= 0:
                        if (x - hailstone_b.px) / hailstone_b.vx >= 0:
                            if min_pos <= x <= max_pos and min_pos <= y <= max_pos:
                                count += 1
    return count


def part_2(hailstones):
    return hailstones


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]

    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    hailstones = []
    for line in lines:
        hailstones.append(Hailstone(line))

    if args.part == 1:
        result = part_1(hailstones, args.test)
    else:
        result = part_2(hailstones)

    print(f"part{args.part} results in {result}")


if __name__ == "__main__":
    main()
