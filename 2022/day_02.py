import argparse
import os
import re

# A,X,1: "Rock",
# B,Y,2: "Paper",
# C,Z,3: "Scissor"

# rock > scissor    1 > 3
# scissor > paper   3 > 2
# paper > rock      2 > 1


def part_1(objects):
    score_b = 0
    for object_set in objects:
        obj_a = object_set[0]
        obj_b = object_set[1]

        # player b
        win_score_b = 0
        if obj_b == obj_a:
            # draw
            win_score_b = 3
        elif (obj_b == 1 and obj_a == 3) or (obj_b == 2 and obj_a == 1) or (obj_b == 3 and obj_a == 2):
            # somebody wins
            win_score_b = 6
        score_b += obj_b + win_score_b
    return score_b


def part_2(objects):
    score_b = 0
    for object_set in objects:
        obj_a = object_set[0]
        obj_b = object_set[1]

        obj_score = 0
        if obj_b == 1:
            # loose
            if obj_a == 1:
                obj_score = 3
            if obj_a == 2:
                obj_score = 1
            if obj_a == 3:
                obj_score = 2
            win_score_b = 0
        if obj_b == 2:
            # draw
            obj_score = obj_a
            win_score_b = 3
        if obj_b == 3:
            # win
            if obj_a == 1:
                obj_score = 2
            if obj_a == 2:
                obj_score = 3
            if obj_a == 3:
                obj_score = 1
            win_score_b = 6
        score_b += obj_score + win_score_b
    return score_b


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    objects = []
    for line in lines:
        objs = re.search(r"([ABC]) ([XYZ])", line)
        obj_a = ord(objs.group(1)) - ord("A") + 1
        obj_b = ord(objs.group(2)) - ord("X") + 1
        objects.append([obj_a, obj_b])

    if args.part == 1:
        result = part_1(objects)
        print(f"part1 results in {result}")
    else:
        result = part_2(objects)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
