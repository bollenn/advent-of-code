import argparse
import os
import sys


SNAFU_DIGITS = {
  "=": -2,
  "-": -1,
  "0": 0,
  "1": 1,
  "2": 2,
}


def snafu_to_dec(snafu_number):
    retval = 0
    for i in range(1, len(snafu_number) + 1):
        dec_digit = SNAFU_DIGITS[snafu_number[-i]]
        retval += dec_digit * pow(5, i - 1)
    return retval


def dec_to_snafu(dec_number):
    snafu_factor = 0
    while True:
        if -2 <= dec_number // pow(5, snafu_factor) <= 2:
            # highest snafu digit found
            break
        snafu_factor += 1
    # now convert dec to snafu
    retval = ""
    number = dec_number
    while snafu_factor >= 0:
        # find smallest abs diff
        min_diff = sys.maxsize
        min_digit = -3
        min_char = ""
        for snafu_char, dec_digit in SNAFU_DIGITS.items():
            digit_diff = abs(number - (dec_digit * pow(5, snafu_factor)))
            if digit_diff < min_diff:
                min_diff = digit_diff
                min_digit = dec_digit
                min_char = snafu_char
        retval += min_char
        number = number - (min_digit * pow(5, snafu_factor))
        snafu_factor -= 1
    return retval


def part_1(numbers):
    retval = 0
    for number in numbers:
        retval += snafu_to_dec(number)
    return dec_to_snafu(retval)


def part_2(numbers):
    return 0


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    numbers = []
    for line in lines:
        numbers.append(line.strip())

    if args.part == 1:
        result = part_1(numbers)
        print(f"part1 results in {result}")
    else:
        result = part_2(numbers)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
