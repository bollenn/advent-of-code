import argparse
import re
import os


class Coordinate():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"[{self.x}, {self.y }]"

    def __eq__(self, other):
        if not isinstance(other, Coordinate):
            return NotImplemented
        return self.x == other.x and self.y == other.y

    def __add__(self, other):
        if not isinstance(other, Coordinate):
            return NotImplemented
        x = self.x + other.x
        y = self.y + other.y
        return Coordinate(x, y)

    def __iadd__(self, other):
        if not isinstance(other, Coordinate):
            return NotImplemented
        self.x += other.x
        self.y += other.y
        return self


class LineSet():
    def __init__(self, line):
        re_matches = re.findall(r"(?: -> ?)?(\d*),(\d*)", line)
        self.coordinates = []
        for match in re_matches:
            self.coordinates.append(Coordinate(int(match[0]), int(match[1])))

    def __repr__(self):
        return f"LineSet with coordinates {self.coordinates}"

    @property
    def min_x(self):
        return min([coorinate.x for coorinate in self.coordinates])

    @property
    def max_x(self):
        return max([coorinate.x for coorinate in self.coordinates])

    @property
    def min_y(self):
        return min([coorinate.y for coorinate in self.coordinates])

    @property
    def max_y(self):
        return max([coorinate.y for coorinate in self.coordinates])


class RockMap():
    MOVING = 0
    STOPPED = 1
    DROPPED = 2

    def __init__(self, linesets):
        self.linesets = linesets
        self._x_min = min([lineset.min_x for lineset in self.linesets])
        self._x_max = max([lineset.max_x for lineset in self.linesets])
        self._y_min = 0
        self._y_max = max([lineset.max_y for lineset in self.linesets])
        self.plan = [["."] * self.width for _ in range(self.height)]

    @property
    def width(self):
        return self._x_max - self._x_min + 1

    @property
    def height(self):
        return self._y_max - self._y_min + 1

    def __repr__(self):
        map_data = ""
        for row in self.plan:
            rowdata = "".join(f"{value}" for value in row)
            map_data += f"{rowdata}\n"
        return map_data

    def _extend_plan_x(self, x_len):
        if x_len > 0:
            self._x_max += x_len
            for line in self.plan:
                for _ in range(x_len):
                    line.append(".")
        else:
            self._x_min += x_len
            for line in self.plan:
                for _ in range(abs(x_len)):
                    line.insert(0, ".")

    def location_on_map(self, location):
        return self._x_min <= location.x <= self._x_max and self._y_min <= location.y <= self._y_max

    def place_item(self, location, item):
        self.plan[location.y - self._y_min][location.x - self._x_min] = item

    def get_item(self, location, offset):
        item_loc = location + offset
        if self.location_on_map(item_loc):
            return self.plan[item_loc.y - self._y_min][item_loc.x - self._x_min]
        return None

    def build_plan(self):
        for lineset in self.linesets:
            origin = lineset.coordinates[0]
            for current in lineset.coordinates[1:]:
                # draw line
                if current.x != origin.x:
                    # horizontal line
                    x_diff = current.x - origin.x
                    x_sign = x_diff // abs(x_diff)
                    for i in range(abs(x_diff) + 1):
                        self.place_item(Coordinate(current.x - (x_sign * i), current.y), "#")
                else:
                    # vertical line
                    y_diff = current.y - origin.y
                    y_sign = y_diff // abs(y_diff)
                    for i in range(abs(y_diff) + 1):
                        self.place_item(Coordinate(current.x, current.y - (y_sign * i)), "#")
                origin = current

    def insert_bottom(self):
        before_line = ["."] * len(self.plan[0])
        bottom_line = ["#"] * len(self.plan[0])
        self.plan.append(before_line)
        self.plan.append(bottom_line)
        self._y_max += 2

    def _do_next_sand_grain_move(self, location):
        type_lbelow = self.get_item(location, Coordinate(-1, 1))
        type_below = self.get_item(location, Coordinate(0, 1))
        type_rbelow = self.get_item(location, Coordinate(1, 1))
        if type_below == ".":
            # vertical drop
            location.y += 1
        elif type_below is None:
            # vertical out of map
            return self.DROPPED
        elif type_lbelow == ".":
            location.x -= 1
            location.y += 1
        elif type_lbelow is None:
            # diagonal left drop out of map
            return self.DROPPED
        elif type_rbelow == ".":
            # diagonal right drop
            location.x += 1
            location.y += 1
        elif type_rbelow is None:
            # diagonal right drop out of map
            return self.DROPPED
        else:
            # stopped moving
            self.place_item(location, "o")
            return self.STOPPED
        return self.MOVING

    def drop_sand_grain(self, origin, do_part2):
        result = self.MOVING
        location = Coordinate(origin.x, origin.y)
        while result == self.MOVING:
            if do_part2:
                if location.x < self._x_min + 2:
                    self._extend_plan_x(-1)
                    self.plan[-1][0] = "#"
                if location.x > self._x_max - 2:
                    self._extend_plan_x(1)
                    self.plan[-1][-1] = "#"
            result = self._do_next_sand_grain_move(location)
        if origin == location:
            # the map is full
            return result == self.DROPPED, True
        return result == self.DROPPED, False


def part1(plan):
    off_map = False
    counter = 0
    while not off_map:
        off_map, _ = plan.drop_sand_grain(Coordinate(500, 0), False)
        counter += 1
    print(plan)
    return counter - 1  # -1 since last one dropped of the board


def part2(plan):
    plan.insert_bottom()
    full = False
    counter = 0
    while not full:
        _, full = plan.drop_sand_grain(Coordinate(500, 0), True)
        counter += 1
    print(plan)
    return counter


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    linesets = []
    for line in lines:
        linesets.append(LineSet(line))

    plan = RockMap(linesets)
    plan.build_plan()

    if args.part == 1:
        result = part1(plan)
        print(f"part1 results in {result}")
    else:
        result = part2(plan)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
