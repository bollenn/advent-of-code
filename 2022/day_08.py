import argparse
import os


def part_1(trees, threshold=0):
    visible_trees = 0
    trees_transposed = [*zip(*trees)]
    for row_nr, treerow in enumerate(trees):
        for tree_nr, tree_height in enumerate(treerow):
            visible = False
            treecolumn = trees_transposed[tree_nr]
            # visible from left?
            highest = threshold
            for tree in treerow[:tree_nr]:
                if tree > highest:
                    highest = tree
            if tree_height > highest:
                visible = True

            # visible from right?
            highest = threshold
            for tree in reversed(treerow[tree_nr+1:]):
                if tree > highest:
                    highest = tree
            if tree_height > highest:
                visible = True

            # visible from top
            highest = threshold
            for tree in treecolumn[:row_nr]:
                if tree > highest:
                    highest = tree
            if tree_height > highest:
                visible = True

            # visible from bottom
            highest = threshold
            for tree in reversed(treecolumn[row_nr+1:]):
                if tree > highest:
                    highest = tree
            if tree_height > highest:
                visible = True

            if visible:
                visible_trees += 1
    return visible_trees


def part_2(trees):
    max_scenic_score = 0
    trees_transposed = [*zip(*trees)]
    for row_nr, treerow in enumerate(trees):
        for tree_nr, tree_height in enumerate(treerow):
            scenic_score = 1
            treecolumn = trees_transposed[tree_nr]

            # visible trees from left?
            visible_trees = 0
            for tree in reversed(treerow[:tree_nr]):
                visible_trees += 1
                if tree >= tree_height:
                    break
            scenic_score *= visible_trees

            # visible from right?
            visible_trees = 0
            for tree in treerow[tree_nr+1:]:
                visible_trees += 1
                if tree >= tree_height:
                    break
            scenic_score *= visible_trees

            # visible from top
            visible_trees = 0
            for tree in reversed(treecolumn[:row_nr]):
                visible_trees += 1
                if tree >= tree_height:
                    break
            scenic_score *= visible_trees

            # visible from bottom
            visible_trees = 0
            for tree in treecolumn[row_nr+1:]:
                visible_trees += 1
                if tree >= tree_height:
                    break
            scenic_score *= visible_trees

            if scenic_score > max_scenic_score:
                max_scenic_score = scenic_score
    return max_scenic_score


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    trees = []
    for line in lines:
        line = line.strip()
        line_trees = []
        for tree in line:
            line_trees.append(int(tree))
        trees.append(line_trees)

    if args.part == 1:
        result = part_1(trees, -1)
        print(f"part1 results in {result}")
    else:
        result = part_2(trees)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
