import argparse
import json
import os


class Packet:
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        return self._check_packet(self.value, other.value) == 0

    def __ne__(self, other):
        return self._check_packet(self.value, other.value) != 0

    def __lt__(self, other):
        return self._check_packet(self.value, other.value) < 0

    def __le__(self, other):
        return self._check_packet(self.value, other.value) <= 0

    def __gt__(self, other):
        return self._check_packet(self.value, other.value) > 0

    def __ge__(self, other):
        return self._check_packet(self.value, other.value) >= 0

    def _check_packet(self, packet1, packet2):
        for pos, subpack in enumerate(packet1):
            if pos < len(packet2):
                if isinstance(subpack, list) or isinstance(packet2[pos], list):
                    # at least on is list so make sure both are
                    if isinstance(subpack, int):
                        subpack = [subpack]
                    if isinstance(packet2[pos], int):
                        packet2[pos] = [packet2[pos]]
                    # compare the both lists
                    temp = self._check_packet(subpack, packet2[pos])
                    if temp != 0:
                        return temp
                else:
                    if subpack > packet2[pos]:
                        # Right side is smaller, so inputs are not in the right order
                        return -1
                    if subpack < packet2[pos]:
                        # Left side is smaller, so inputs are in the right order
                        return 1
            else:
                # Right side ran out of items, so inputs are not in the right order
                return -1
        if len(packet1) < len(packet2):
            # Left side ran out of items, so inputs are in the right order
            return 1
        return 0

    def __repr__(self):
        return f"{self.value}"


def part1(packet_pairs):
    correct_packs = []
    for packet_nr, packet_pair in enumerate(packet_pairs):
        if packet_pair[0] >= packet_pair[1]:
            correct_packs.append(packet_nr + 1)
    return sum(correct_packs)


def part2(packet_pairs):
    # we are not checking pairs so convert to list
    packets = []
    for packet1, packet2 in packet_pairs:
        packets.append(packet1)
        packets.append(packet2)

    # insert decoder keys
    decoder_key_1 = Packet([2])
    decoder_key_2 = Packet([6])

    packets.append(decoder_key_1)
    packets.append(decoder_key_2)

    packets.sort(reverse=True)

    # get locations of decoder keys in sorted list
    index_key_1 = packets.index(decoder_key_1) + 1
    index_key_2 = packets.index(decoder_key_2) + 1

    return index_key_1 * index_key_2


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    packet_pairs = []
    cur_pair = []
    for line in lines:
        line = line.strip()
        if line != "":
            cur_pair.append(Packet(json.loads(line)))
        else:
            packet_pairs.append(cur_pair)
            cur_pair = []

    packet_pairs.append(cur_pair)

    if args.part == 1:
        result = part1(packet_pairs)
        print(f"part1 results in {result}")
    else:
        result = part2(packet_pairs)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
