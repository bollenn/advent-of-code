import argparse
import os


class Instruction:
    def __init__(self, line):
        line = line.strip().split(" ")
        self.command = line[0]
        self.value = int(line[1]) if len(line) > 1 else 0

    def __repr__(self):
        return f"instruction={self.command} {self.value}"


class Sprite:
    def __init__(self):
        self.pixels = [["."]*40 for _ in range(6)]

    def __repr__(self):
        result = ""
        for row in self.pixels:
            result += f"{''.join(pixel for pixel in row)}\n"
        return result

    def update_pixels(self, cycles, x_reg):
        cycles -= 1  # cycles have to be 0 ref
        if x_reg - 1 <= cycles % 40 <= x_reg + 1:
            self.pixels[cycles // 40][cycles % 40] = "#"


class Cpu:
    def __init__(self, instructions, sprite):
        self.cycles = 1
        self.x_reg = 1
        self.instructions = instructions
        self.pc = 0
        self.sprite = sprite

    def __repr__(self):
        return f"cycles={self.cycles} X={self.x_reg} signal_strength={self.signal_strength}"

    @property
    def signal_strength(self):
        return self.cycles * self.x_reg

    def update_cycle(self, x_reg_update):
        self.sprite.update_pixels(self.cycles, self.x_reg)
        self.cycles += 1
        self.x_reg += x_reg_update
        if (self.cycles - 20) % 40 == 0:
            return self.signal_strength
        return 0

    def execute_instruction(self):
        signal = 0
        if self.instructions[self.pc].command == "noop":
            signal += self.update_cycle(0)
        if self.instructions[self.pc].command == "addx":
            signal += self.update_cycle(0)
            signal += self.update_cycle(self.instructions[self.pc].value)
        self.pc += 1
        if self.pc >= len(self.instructions):
            return signal, False
        return signal, True


def part_x(cpu):
    sum_signal = 0
    cpu_running = True
    while cpu_running:
        signal, cpu_running = cpu.execute_instruction()
        sum_signal += signal
    return sum_signal


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    instructions = []
    for line in lines:
        instructions.append(Instruction(line))

    sprite = Sprite()
    cpu = Cpu(instructions, sprite)

    result = part_x(cpu)
    print(f"part1 results in {result}")
    print("part2 result:")
    print(sprite)


if __name__ == "__main__":
    main()
