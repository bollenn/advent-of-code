import argparse
import os
import sys


DIRECTIONS = [
    [[0, -1], [[0, -1], [1, -1], [-1, -1]]],  # move north
    [[0, 1], [[0, 1], [1, 1], [-1, 1]]],      # move south
    [[-1, 0], [[-1, 0], [-1, -1], [-1, 1]]],  # move west
    [[1, 0], [[1, 0], [1, -1], [1, 1]]],      # move east
]


def get_elves_around_me(elve, elves):
    close_neighbours = []
    for neigh_elve in elves:
        if elve != neigh_elve:
            if elve.x - 2 <= neigh_elve.x <= elve.x + 2 and elve.y - 2 <= neigh_elve.y <= elve.y + 2:
                close_neighbours.append(neigh_elve)
    return close_neighbours


def check_alone(elve, elves):
    for neigh_elve in elves:
        if elve != neigh_elve:
            if elve.x - 1 <= neigh_elve.x <= elve.x + 1 and elve.y - 1 <= neigh_elve.y <= elve.y + 1:
                return False
    return True


def check_empty(x, y, elves):
    for neigh_elve in elves:
        if neigh_elve.x == x and neigh_elve.y == y:
            return False
    return True


def check_no_duplicate(elve, elves):
    for neigh_elve in elves:
        if elve != neigh_elve:
            if neigh_elve.next_x == elve.next_x and neigh_elve.next_y == elve.next_y:
                return False
    return True


class Elve:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.next_x = x
        self.next_y = y
        self.dir_pos = 0
        self.close_neighbours = None

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def get_scan_dir(self, offset):
        pos = self.dir_pos + offset
        while pos >= len(DIRECTIONS):
            pos -= len(DIRECTIONS)
        return DIRECTIONS[pos]

    def estimate_next_pos(self, elves):
        # reset
        self.next_x = self.x
        self.next_y = self.y
        self.close_neighbours = get_elves_around_me(self, elves)
        if not check_alone(self, self.close_neighbours):
            # we are not alone, let scan for a new location
            for i in range(len(DIRECTIONS)):
                new_dir, new_scan = self.get_scan_dir(i)
                empty = True
                for scan_pos in new_scan:
                    if not check_empty(self.x + scan_pos[0], self.y + scan_pos[1], self.close_neighbours):
                        empty = False
                        break
                if empty:
                    self.next_x = self.x + new_dir[0]
                    self.next_y = self.y + new_dir[1]
                    return

    def do_move(self):
        moved = False
        if self.x != self.next_x or self.y != self.next_y:
            if check_no_duplicate(self, self.close_neighbours):
                self.x = self.next_x
                self.y = self.next_y
                moved = True
        self.dir_pos += 1
        if self.dir_pos >= len(DIRECTIONS):
            self.dir_pos = 0
        return moved


class ElveChart:
    def __init__(self, all_elves):
        self.elves = all_elves

    def __repr__(self):
        min_x, min_y, max_x, max_y = self.get_area()
        min_x = min(0, min_x)
        min_y = min(0, min_y)
        retval = ""
        for y in range(min_y, max_y + 1):
            line_info = ""
            for x in range(min_x, max_x + 1):
                if self.get_elve_at_location(x, y) is not None:
                    line_info += "#"
                else:
                    line_info += "."
            retval += f"{line_info}\n"
        return retval

    def get_elve_at_location(self, x, y):
        for elve in self.elves:
            if elve.x == x and elve.y == y:
                return elve
        return None

    def count_empty(self):
        min_x, min_y, max_x, max_y = self.get_area()
        empty_nr = 0
        for y in range(min_y, max_y + 1):
            for x in range(min_x, max_x + 1):
                if self.get_elve_at_location(x, y) is None:
                    empty_nr += 1
        return empty_nr

    def get_area(self):
        min_x = sys.maxsize
        min_y = sys.maxsize
        max_x = 0
        max_y = 0
        for elve in self.elves:
            min_x = min(min_x, elve.x)
            min_y = min(min_y, elve.y)
            max_x = max(max_x, elve.x)
            max_y = max(max_y, elve.y)
        return min_x, min_y, max_x, max_y

    def do_first_half(self):
        for elve in self.elves:
            elve.estimate_next_pos(self.elves)

    def do_second_half(self):
        elves_moved = False
        for elve in self.elves:
            if elve.do_move():
                elves_moved = True
        return elves_moved


def part_x(chart, part2):
    elves_moved = True
    count = 0
    while elves_moved:
        chart.do_first_half()
        elves_moved = chart.do_second_half()
        count += 1
        if count >= 10 and not part2:
            # count empty tiles
            return chart.count_empty()
    return count


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    all_elves = []
    for y, line in enumerate(lines):
        line = line.strip()
        for x, char in enumerate(line):
            if char == "#":
                elve = Elve(x, y)
                all_elves.append(elve)

    chart = ElveChart(all_elves)

    if args.part == 1:
        result = part_x(chart, False)
        print(f"part1 results in {result}")
    else:
        result = part_x(chart, True)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
