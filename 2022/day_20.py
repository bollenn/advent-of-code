import argparse
import os


class Number:
    def __init__(self, value, first):
        self.value = value
        self.next_nr = None
        self.previous_nr = None
        self.first_item = first

    def __repr__(self):
        return f"{self.value}"


def print_numbers(numbers):
    first_nr = None
    for number in numbers:
        if number.first_item:
            first_nr = number
            break
    curr_nr = first_nr
    for _ in range(len(numbers)):
        print(f"{curr_nr} --> p={curr_nr.previous_nr} n={curr_nr.next_nr}")
        curr_nr = curr_nr.next_nr
    print()


def move_nr(number, numbers):
    pos_to_move = number.value
    # remove non effecting loops
    pos_to_move %= len(numbers) - 1
    # let do real moving
    if pos_to_move > 0:
        # move up
        while pos_to_move > 0:
            next_nr_copy = number.next_nr
            prev_copy = number.previous_nr
            prev_copy.next_nr = next_nr_copy
            next_nr_copy.previous_nr = prev_copy
            number.next_nr = next_nr_copy.next_nr
            number.previous_nr = next_nr_copy
            number.next_nr.previous_nr = number
            next_nr_copy.next_nr = number
            if number.first_item:
                number.first_item = False
                next_nr_copy.first_item = True
            pos_to_move -= 1
    else:
        # move back
        while pos_to_move < 0:
            next_nr_copy = number.next_nr
            prev_copy = number.previous_nr
            next_nr_copy.previous_nr = prev_copy
            prev_copy.next_nr = next_nr_copy
            number.next_nr = prev_copy
            number.previous_nr = prev_copy.previous_nr
            number.previous_nr.next_nr = number
            prev_copy.previous_nr = number
            if number.first_item:
                number.first_item = False
                next_nr_copy.first_item = True
            pos_to_move += 1


def part_1(numbers):
    # search for the zero
    zero = None
    for number in numbers:
        if number.value == 0:
            zero = number
            break

    # move the numbers
    for number in numbers:
        move_nr(number, numbers)

    # determine the grove coordinates
    curr_nr = zero
    for _ in range(1000):
        curr_nr = curr_nr.next_nr
    groove_coord = curr_nr.value
    for _ in range(1000):
        curr_nr = curr_nr.next_nr
    groove_coord += curr_nr.value
    for _ in range(1000):
        curr_nr = curr_nr.next_nr
    groove_coord += curr_nr.value

    return groove_coord


def part_2(numbers):
    # search for the zero and apply decryption key
    zero = None
    for number in numbers:
        if number.value == 0:
            zero = number
        else:
            number.value *= 811589153

    # move the numbers
    for _ in range(10):
        for number in numbers:
            move_nr(number, numbers)

    # determine the grove coordinates
    curr_nr = zero
    for _ in range(1000):
        curr_nr = curr_nr.next_nr
    groove_coord = curr_nr.value
    for _ in range(1000):
        curr_nr = curr_nr.next_nr
    groove_coord += curr_nr.value
    for _ in range(1000):
        curr_nr = curr_nr.next_nr
    groove_coord += curr_nr.value

    return groove_coord


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    numbers = []
    first_not_done = True
    for line in lines:
        numbers.append(Number(int(line), first_not_done))
        first_not_done = False

    for pos, number in enumerate(numbers):
        number.next_nr = numbers[pos+1 if pos+1 < len(numbers) else 0]
        number.previous_nr = numbers[pos-1 if pos-1 >= 0 else len(numbers)-1]

    if args.part == 1:
        result = part_1(numbers)
        print(f"part1 results in {result}")
    else:
        result = part_2(numbers)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
