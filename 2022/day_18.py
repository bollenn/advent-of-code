import argparse
import re
import os

# x, y, z
cube_sides = [
    [1, 0, 0],
    [-1, 0, 0],
    [0, 1, 0],
    [0, -1, 0],
    [0, 0, 1],
    [0, 0, -1]
]


def insert_steam(space):
    filled = False
    for z_nr, layer in enumerate(space):
        for y_nr, line in enumerate(layer):
            for x_nr, pos in enumerate(line):
                if pos == 0 and check_neighbours(x_nr, y_nr, z_nr, space, 2):
                    space[z_nr][y_nr][x_nr] = 2
                    filled = True
    return filled


def check_neighbours(x, y, z, space, value):
    for side in cube_sides:
        if z+side[2] >= len(space) or y+side[1] >= len(space[0]) or x+side[0] >= len(space[0][0]):
            return True
        if space[z+side[2]][y+side[1]][x+side[0]] == value:
            return True
    return False


class Cube:
    def __init__(self, line):
        re_match = re.search(r"(\d*),(\d*),(\d*)", line)
        self.x = int(re_match.group(1))
        self.y = int(re_match.group(2))
        self.z = int(re_match.group(3))

    def __repr__(self):
        return f"Cube at {self.x},{self.y},{self.z}"

    def check_free_air_sides(self, space, compare):
        p1_free_air_sides = 0
        for side in cube_sides:
            if space[self.z + side[2]][self.y + side[1]][self.x + side[0]] == compare:
                p1_free_air_sides += 1
        return p1_free_air_sides


def part_x(cubes):
    # start with defining the space
    min_x = 0
    max_x = 0
    min_y = 0
    max_y = 0
    min_z = 0
    max_z = 0
    for cube in cubes:
        min_x = min(min_x, cube.x)
        max_x = max(max_x, cube.x + 2)  # +2 to allow steam arround
        min_y = min(min_y, cube.y)
        max_y = max(max_y, cube.y + 2)
        min_z = min(min_z, cube.z)
        max_z = max(max_z, cube.z + 2)

    # generate space and fill with air (0)
    space = [[[0] * (max_x - min_x) for y in range(max_y - min_y + 1)] for z in range(max_z - min_z + 1)]

    # fill space with cubes (1)
    for cube in cubes:
        space[cube.z - min_z][cube.y - min_y][cube.x - min_x] = 1

    # get part 1 result
    p1_result = 0
    for cube in cubes:
        p1_free_air_sides = cube.check_free_air_sides(space, 0)
        p1_result += p1_free_air_sides

    # fill space with steam (2)
    space[0][0][0] = 2
    busy = True
    while busy:
        busy = insert_steam(space)

    # get part 2 result
    p2_result = 0
    for cube in cubes:
        p2_free_air_sides = cube.check_free_air_sides(space, 2)
        p2_result += p2_free_air_sides

    print(f"part1 results in {p1_result}")
    print(f"part2 results in {p2_result}")


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    cubes = []
    for line in lines:
        cubes.append(Cube(line))

    part_x(cubes)


if __name__ == "__main__":
    main()
