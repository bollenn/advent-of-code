import argparse
import os
import sys

sys.setrecursionlimit(1500)


class Node:
    def __init__(self, column, row, elevation):
        self.column = column
        self.row = row
        self.neighbours = []
        self.distance = None
        self.path_node = None
        if elevation == "S":
            self.elevation = 0
            self.type = "start"
        elif elevation == "E":
            self.elevation = ord("z") - ord("a")
            self.type = "end"
        else:
            self.elevation = ord(elevation) - ord("a")
            self.type = "normal"

    def reset(self):
        self.distance = None
        self.path_node = None

    def fill_neighbours(self, nodes):
        for node in nodes:
            if (((self.column - 1 <= node.column <= self.column + 1 and node.row == self.row) or
                 (node.column == self.column and self.row - 1 <= node.row <= self.row + 1)) and
                 (node.elevation <= self.elevation + 1)):
                # this is a neighbour of intrest
                self.neighbours.append(node)

    def set_distance(self, new_distance, path_node):
        if self.distance is None or self.distance > new_distance:
            self.distance = new_distance
            self.path_node = path_node
            for neighbour in self.neighbours:
                neighbour.set_distance(self.distance+1, self)

    def get_path(self):
        if self.path_node is not None:
            return f"[{self.column},{self.row}]" + self.path_node.get_path()
        return ""

    def __repr__(self):
        if self.path_node is not None:
            return f"[{self.column},{self.row}] closest = [{self.path_node.column},{self.path_node.row}]"
        return f"[{self.column},{self.row}] is start"


def part1(nodes):
    # get start and end node
    for node in nodes:
        if node.type == "start":
            start_node = node
        elif node.type == "end":
            end_node = node
    # find 'S' node distances to 'E'
    start_node.set_distance(0, None)
    # print path 'S' to 'E'
    print(end_node.get_path())
    # return result
    return end_node.distance


def part2(nodes):
    # get start and end node
    for node in nodes:
        if node.type == "end":
            end_node = node
    # find all 'a' node distances to 'E'
    a_node_distances = []
    for node in nodes:
        if node.elevation == 0:
            node.reset()
            node.set_distance(0, None)
            a_node_distances.append(end_node.distance)
    # return result
    return min(a_node_distances)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    nodes = []
    for row_nr, line in enumerate(lines):
        line = line.strip()
        for column_nr, ele_char in enumerate(line):
            nodes.append(Node(column_nr, row_nr, ele_char))

    # provide all nodes with neighbour node information
    for node in nodes:
        node.fill_neighbours(nodes)

    if args.part == 1:
        result = part1(nodes)
        print(f"part1 results in {result}")
    else:
        result = part2(nodes)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
