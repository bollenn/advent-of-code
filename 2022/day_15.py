import argparse
import re
import sys
import os

NOT_COVERED = 0
COVERED = 1
BEACON = 2
SENSOR = 3
SYMBOL_DICT = {
    0: ".",
    1: "#",
    2: "B",
    3: "S",
}


class Sensor:
    def __init__(self, line):
        re_match = re.search(r"Sensor at x=(-?\d*), y=(-?\d*): closest beacon is at x=(-?\d*), y=(-?\d*)", line)
        self.sensor_x = int(re_match[1])
        self.sensor_y = int(re_match[2])
        self.beacon_x = int(re_match[3])
        self.beacon_y = int(re_match[4])
        self.manhattan_distance = abs(self.sensor_x - self.beacon_x) + abs(self.sensor_y - self.beacon_y)

    def get_point_covered(self, row_nr, column_nr):
        if self.sensor_y - self.manhattan_distance <= row_nr <= self.sensor_y + self.manhattan_distance:
            offset = self.manhattan_distance - abs(row_nr - self.sensor_y)
            if self.sensor_x - offset <= column_nr <= self.sensor_x + offset:
                return True, self.sensor_x + offset
        return False, None

    def get_point_symbol(self, row_nr, column_nr):
        if column_nr == self.sensor_x and row_nr == self.sensor_y:
            return SENSOR
        if column_nr == self.beacon_x and row_nr == self.beacon_y:
            return BEACON
        sensed, _ = self.get_point_covered(row_nr, column_nr)
        if sensed:
            return COVERED
        return NOT_COVERED

    def get_range_for_row(self, row_nr):
        offset = self.manhattan_distance - abs(row_nr - self.sensor_y)
        if offset >= 0:
            return range(self.sensor_x - offset, self.sensor_x + offset + 1)
        return None

    def print(self):
        return f"sensor at {self.sensor_x},{self.sensor_y} with beacon at {self.beacon_x},{self.beacon_y} and manhattan distance {self.manhattan_distance}"

    def __repr__(self):
        return self.print()


def find_boundaries_of_map(sensors):
    min_x = sys.maxsize
    max_x = 0
    min_y = sys.maxsize
    max_y = 0
    for sensor in sensors:
        min_x = min(min_x, sensor.sensor_x, sensor.beacon_x)
        max_x = max(max_x, sensor.sensor_x, sensor.beacon_x)
        min_y = min(min_y, sensor.sensor_y, sensor.beacon_y)
        max_y = max(max_y, sensor.sensor_y, sensor.beacon_y)
    return min_x, max_x, min_y, max_y


def print_map(sensors, min_x, max_x, min_y, max_y):
    row = min_y
    while row <= max_y:
        column = min_x
        row_data = ""
        while column <= max_x:
            symbol = NOT_COVERED
            for sensor in sensors:
                result = sensor.get_point_symbol(row, column)
                if result > symbol:
                    symbol = result
            row_data += SYMBOL_DICT[symbol]
            column += 1
        print(f"{row_data}")
        row += 1
    print()


def part1(sensors, row_nr, printing=False):
    if printing:
        min_x, max_x, min_y, max_y = find_boundaries_of_map(sensors)
        print_map(sensors, min_x, max_x, min_y, max_y)

    line_dict = {}
    beacons_x_on_row = {}
    sensors_x_on_row = {}
    for sensor in sensors:
        sens_range = sensor.get_range_for_row(row_nr)
        if sens_range is not None:
            if sensor.beacon_y == row_nr:
                beacons_x_on_row[sensor.beacon_x] = True
            if sensor.sensor_y == row_nr:
                sensors_x_on_row[sensor.sensor_x] = True
            for i in sens_range:
                line_dict[i] = True
    return len(line_dict) - len(beacons_x_on_row) - len(sensors_x_on_row)


def part2(sensors, detectsize, printing=False):
    if printing:
        min_x, max_x, min_y, max_y = find_boundaries_of_map(sensors)
        min_x = max(min_x, 0)
        max_x = min(max_x, detectsize)
        min_y = max(min_y, 0)
        max_y = min(max_y, detectsize)
        print_map(sensors, min_x, max_x, min_y, max_y)

    for row in range(detectsize+1):
        column = 0
        while column <= detectsize:
            inrange = False
            for sensor in sensors:
                result, last = sensor.get_point_covered(row, column)
                if result:
                    column = last
                    inrange = True
                    break
            if not inrange:
                print(f"found non covered location in [{column}, {row}]")
                print()
                return column * 4000000 + row
            column += 1
    return None


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    sensors = []
    for line in lines:
        sensors.append(Sensor(line))

    if args.part == 1:
        result = part1(sensors, 10 if args.test else 2000000, printing=args.test)
        print(f"part1 results in {result}")
    else:
        result = part2(sensors, 20 if args.test else 4000000, printing=args.test)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
