import argparse
import os
import re
import sys


DIRECTIONS = [
    [1, 0],
    [0, 1],
    [-1, 0],
    [0, -1]
]

FACING_LEFT = 0
FACING_TOP = 1
FACING_RIGHT = 2
FACING_BOTTOM = 3


#    0 1 2 3
# 0      2
# 2  4 5 6
# 3      10 11

# 2T -> 4T
# 2L -> 5T
# 2R -> 11R
# 4L -> 11B
# 4T -> 2T
# 4B -> 10B
# 5T -> 2L
# 5B -> 10L
# 6R -> 11T
# 10L -> 5B
# 10B -> 4B
# 11T -> 6R
# 11R -> 2R
# 11B -> 4L

# [R, B, L, T]
# [next box, facing]
TELEPORT_TEST = [
    None,  # 0
    None,  # 1
    [[11, FACING_RIGHT], [6, FACING_TOP], [5, FACING_TOP], [4, FACING_TOP]],  #2
    None,  # 3
    [[5, FACING_LEFT], [10, FACING_BOTTOM], [11, FACING_BOTTOM], [2, FACING_TOP]],  #4
    [[6, FACING_LEFT], [10, FACING_LEFT], [4, FACING_RIGHT], [2, FACING_LEFT]],  #5
    [[11, FACING_TOP], [10, FACING_TOP], [5, FACING_RIGHT], [2, FACING_BOTTOM]],  #6
    None,  # 7
    None,  # 8
    None,  # 9
    [[11, FACING_LEFT], [4, FACING_BOTTOM], [5, FACING_BOTTOM], [6, FACING_BOTTOM]],  # 10
    [[2, FACING_RIGHT], [4, FACING_LEFT], [10, FACING_RIGHT], [6, FACING_RIGHT]],  # 11
]

#    0 1 2
# 0    1 2
# 1    4
# 2  6 7
# 3  9

# 9R -> 7B
# 9B -> 2T
# 9L -> 1T
# 6T -> 4L
# 6L -> 1L
# 7B -> 9R
# 7R -> 2R
# 4L -> 6T
# 4R -> 2B
# 1L -> 6L
# 1T -> 9L
# 2T -> 9B
# 2R -> 7R
# 2B -> 4R

# [R, B, L, T]
# [next box, facing]
TELEPORT_INPUT = [
    None,  # 0
    [[2, FACING_LEFT], [4, FACING_TOP], [6, FACING_LEFT], [9, FACING_LEFT]],  # 1
    [[7, FACING_RIGHT], [4, FACING_RIGHT], [1, FACING_RIGHT], [9, FACING_BOTTOM]],  #2
    None,  # 3
    [[2, FACING_BOTTOM], [7, FACING_TOP], [6, FACING_TOP], [1, FACING_BOTTOM]],  #4
    None,  #5
    [[7, FACING_LEFT], [9, FACING_TOP], [1, FACING_LEFT], [4, FACING_LEFT]],  #6
    [[2, FACING_RIGHT], [9, FACING_RIGHT], [6, FACING_RIGHT], [4, FACING_BOTTOM]],  # 7
    None,  # 8
    [[7, FACING_BOTTOM], [2, FACING_TOP], [1, FACING_TOP], [6, FACING_BOTTOM]],  # 9
    None,  # 10
    None,  # 11
]


class Chart:
    def __init__(self, chart_data):
        self.chart = []
        self.part2 = False
        for line in chart_data:
            chart_line = []
            for char in line:
                if char in ".# ":
                    chart_line.append(char)
            self.chart.append(chart_line)
        # find x length and try finding block width
        shortest_x = sys.maxsize
        longest_x = 0
        for line in self.chart:
            x_ctr = 0
            longest_x = max(longest_x, len(line))
            for char in line:
                if char in ('.', '#'):
                    x_ctr += 1
                else:
                    if x_ctr > 0:
                        shortest_x = min(x_ctr, shortest_x)
                    x_ctr = 0
            if x_ctr > 0:
                shortest_x = min(x_ctr, shortest_x)
        self.width = longest_x
        self.face_x_cnt = self.width // shortest_x
        self.face_x_len = shortest_x
        self.face_y_len = self.height // (12 // self.face_x_cnt)

    def __repr__(self):
        retval = ""
        for line in self.chart:
            chart_line = "".join(char for char in line)
            retval += f"{chart_line}\n"
        return retval

    @property
    def height(self):
        return len(self.chart)

    def get_start_coord(self):
        # find left most open tile of the top row
        for x, char in enumerate(self.chart[0]):
            if char == ".":
                return x, 0
        return None

    def get_next_coord(self, x, y, facing, teleport):
        direction = DIRECTIONS[facing]
        if not self.part2:
            while True:
                x += direction[0]
                y += direction[1]
                if x < 0:
                    x = self.width - 1
                if x >= self.width:
                    x = 0
                if y < 0:
                    y = self.height - 1
                if y >= self.height:
                    y = 0
                if self.get_groundtype(x, y) != " ":
                    return x, y, facing
        # apply part2 logic
        while True:
            x_new = x + direction[0]
            y_new = y + direction[1]
            if self.get_groundtype(x_new, y_new) != " ":
                return x_new, y_new, facing
            face_nr = (x // self.face_x_len) + self.face_x_cnt * (y // self.face_y_len)
            next_face_nr, next_facing = teleport[face_nr][facing]
            face_offset_x = x % self.face_x_len
            face_offset_y = y % self.face_y_len
            if facing in (FACING_LEFT, FACING_RIGHT):
                offset = face_offset_y
            elif facing in (FACING_TOP, FACING_BOTTOM):
                offset = face_offset_x
            next_face_0_x = (next_face_nr % self.face_x_cnt) * self.face_x_len
            next_face_0_y = (next_face_nr // self.face_x_cnt) * (self.face_y_len)
            if next_facing == FACING_LEFT:
                # walk left to right
                x = next_face_0_x
                if facing == FACING_LEFT:
                    y = next_face_0_y + offset
                if facing == FACING_TOP:
                    y = next_face_0_y + self.face_y_len - 1 - offset
                elif facing == FACING_RIGHT:
                    y = next_face_0_y + self.face_y_len - 1 - offset
                elif facing == FACING_BOTTOM:
                    y = next_face_0_y + offset
            elif next_facing == FACING_TOP:
                # walk top to bottom
                if facing == FACING_LEFT:
                    x = next_face_0_x + self.face_x_len - 1 - offset
                if facing == FACING_TOP:
                    x = next_face_0_x + offset
                elif facing == FACING_RIGHT:
                    x = next_face_0_x + offset
                elif facing == FACING_BOTTOM:
                    x = next_face_0_x + self.face_x_len - 1 - offset
                y = next_face_0_y
            elif next_facing == FACING_RIGHT:
                # walk right to left
                x = next_face_0_x + self.face_x_len - 1
                if facing == FACING_LEFT:
                    y = next_face_0_y + self.face_y_len - 1 - offset
                if facing == FACING_TOP:
                    y = next_face_0_y + offset
                elif facing == FACING_RIGHT:
                    y = next_face_0_y + offset
                elif facing == FACING_BOTTOM:
                    y = next_face_0_y + self.face_y_len - 1 - offset
            elif next_facing == FACING_BOTTOM:
                # walk bottom to top
                if facing == FACING_LEFT:
                    x = next_face_0_x + offset
                if facing == FACING_TOP:
                    x = next_face_0_x + self.face_x_len - 1 - offset
                elif facing == FACING_RIGHT:
                    x = next_face_0_x + self.face_x_len - 1 - offset
                elif facing == FACING_BOTTOM:
                    x = next_face_0_x + offset
                y = next_face_0_y + self.face_y_len - 1
            return x, y, next_facing

    def get_groundtype(self, x, y):
        if 0 <= x < self.width and 0 <= y < self.height:
            try:
                groundtype = self.chart[y][x]
            except IndexError:
                groundtype = " "
            return groundtype
        if self.part2:
            return " "
        raise IndexError(f"{x},{y} out of range")


class Path:
    def __init__(self, path_data):
        self.path = []
        for line in path_data:
            line = line.strip()
            re_match = re.findall(r"(\d*)([RL])?", line)
            for item in re_match:
                if item[0] != "":
                    self.path.append(item[0])
                if item[1] != "":
                    self.path.append(item[1])

    def __repr__(self):
        return "".join(f"{step} " for step in self.path)


def walk_the_path(chart, path, teleport=None):
    x, y = chart.get_start_coord()
    print(f"\nstart position is at {x + 1},{y + 1}")

    facing = 0
    for step in path.path:
        step_nr = -1
        try:
            step_nr = int(step)
        except ValueError as e:
            if step == "R":
                facing += 1
                if facing >= len(DIRECTIONS):
                    facing = 0
            elif step == "L":
                facing -= 1
                if facing < 0:
                    facing = len(DIRECTIONS) - 1
            else:
                raise e
        # do stepping
        while step_nr > 0:
            step_nr -= 1
            x_prev = x
            y_prev = y
            facing_prev = facing
            x, y, facing = chart.get_next_coord(x, y, facing, teleport)
            groundtype = chart.get_groundtype(x, y)
            if groundtype == "#":
                # this is a wall, return 1 and stop there
                x = x_prev
                y = y_prev
                facing = facing_prev
                step_nr = 0
            elif groundtype == ".":
                # this is walkable ground, continue
                pass
            elif groundtype == " ":
                # this is a gap, loop till next
                step_nr += 1

    print(f"end position is at {x},{y} facing {facing}\n")
    return x, y, facing


def part_1(chart, path):
    print(chart)
    x, y, facing = walk_the_path(chart, path)
    return 1000 * (y + 1) + 4 * (x + 1) + facing


def part_2(chart, path, test):
    print(chart)

    chart.part2 = True

    if test:
        teleport = TELEPORT_TEST
    else:
        teleport = TELEPORT_INPUT

    x, y, facing = walk_the_path(chart, path, teleport=teleport)
    return 1000 * (y + 1) + 4 * (x + 1) + facing


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    chart_data = []
    path_data = []
    reading_chart = True
    for line in lines:
        line = line.replace("\n", "")
        if line != "":
            if reading_chart:
                chart_data.append(line)
            else:
                path_data.append(line)
        else:
            reading_chart = False

    chart = Chart(chart_data)
    path = Path(path_data)

    if args.part == 1:
        result = part_1(chart, path)
        print(f"part1 results in {result}")
    else:
        result = part_2(chart, path, args.test)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
