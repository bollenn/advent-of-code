import argparse
import re
import os
import copy


class Valve:
    def __init__(self, line):
        re_match = re.search(r"Valve (\S{2}) has flow rate=(\d*); tunnels? leads? to valves? (.*)\n", line)
        self.location = re_match[1]
        self.flow_rate = int(re_match[2])
        self.neighbours_locs = re.findall(r"(\S{2})", re_match[3])
        self.neighbours = []
        self.distances = {}
        self.distances[self.location] = 0

    def __repr__(self):
        return f"Valve = {self.location} flow_rate = {self.flow_rate} neighbours = {self.neighbours_locs}"

    def fill_neighbours(self, valves):
        self.neighbours = []
        valve_locations = [valve.location for valve in valves]
        for location in self.neighbours_locs:
            self.neighbours.append(valves[valve_locations.index(location)])
            self.distances[location] = 1

    def fill_distances(self):
        updated = False
        for valve in self.neighbours:
            for location, distance in valve.distances.items():
                if location not in self.distances:
                    self.distances[location] = distance + 1
                    updated = True
        return updated

    def get_distance_to_valve(self, next_valve):
        return self.distances[next_valve.location]


def generate_paths(total_time, time, start_valve, valves, all_paths, valve_path=None, flow_rate=0, pressure=0):
    if valve_path is None:
        valve_path = []
    valve_path_copy = copy.copy(valve_path)
    valve_path_copy.append(start_valve)
    my_flow_rate = flow_rate + start_valve.flow_rate
    if len(valves) == 0:
        my_pressure = pressure + (my_flow_rate * (total_time - time))
        all_paths[my_pressure] = valve_path_copy
    else:
        for valve in valves:
            distance = start_valve.get_distance_to_valve(valve) + 1
            if time + distance > total_time:
                my_pressure = pressure + (my_flow_rate * (total_time - time))
                all_paths[my_pressure] = valve_path_copy
            else:
                valves_copy = copy.copy(valves)
                valves_copy.remove(valve)
                my_pressure = pressure + (my_flow_rate * distance)
                generate_paths(total_time, time + distance, valve, valves_copy, all_paths, valve_path_copy, my_flow_rate, my_pressure)


def part1(all_valves, start_valve):
    # get a list of all valves which at least generate some flow
    valves_of_interest = []
    for valve in all_valves:
        if valve.flow_rate > 0:
            valves_of_interest.append(valve)

    # generate all paths and their pressure which are possible in 30min
    all_paths = {}
    generate_paths(30, 0, start_valve, valves_of_interest, all_paths)

    # find the best path
    pressure1 = max(all_paths.keys())
    path1 = all_paths[pressure1]
    print([p.location for p in path1])

    return pressure1


def part2(all_valves, start_valve):

    return 0


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    valves = []
    for line in lines:
        valves.append(Valve(line))

    for valve in valves:
        valve.fill_neighbours(valves)

    # now that all valves know their neighbour valve object make sure they have a distance table
    dist_updated = True
    while dist_updated:
        dist_updated = False
        for valve in valves:
            if valve.fill_distances():
                dist_updated = True

    start_valve = valves[[valve.location for valve in valves].index("AA")]

    if args.part == 1:
        result = part1(valves, start_valve)
        print(f"part1 results in {result}")
    else:
        result = part2(valves, start_valve)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
