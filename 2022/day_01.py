import argparse
import os


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    elves = []
    calories = 0
    max_cal = 0
    for line in lines:
        line = line.strip()
        if line:
            calories += int(line)
        else:
            # next elve
            elves.append(calories)
            if calories > max_cal:
                max_cal = calories
            calories = 0

    if calories != 0:
        elves.append(calories)

    elves.sort()

    print(f"part1 results in {max_cal}")
    print(f"part2 results in {sum(elves[-3:])}")


if __name__ == "__main__":
    main()
