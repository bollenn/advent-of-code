import argparse
import os
import re
import sympy


class Monkey:
    def __init__(self, line):
        re_match = re.search(r"(.*):\s(.*)", line)
        self.name = re_match.group(1)
        expr_match = re.search(r"(.*)\s(.)\s(.*)", re_match.group(2))
        if expr_match is not None:
            # this is a waiting monkey
            self.waiting = [expr_match.group(1), expr_match.group(3)]
            self.operation = expr_match.group(2)
            self.value = None
        else:
            # this is a number yelling monkey
            self.waiting = []
            self.operation = None
            self.value = int(re_match.group(2))

    def __repr__(self):
        if self.value is not None:
            return f"{self.name} yelling {self.value}"
        return f"{self.name} waiting for {self.waiting}"

    def get_expression(self):
        if self.value is not None:
            return f"{self.value}"
        return f"({self.waiting[0]} {self.operation} {self.waiting[1]})"


def update_expression(monkeys, expression):
    updated = True
    while updated:
        updated = False
        for monkey in monkeys.values():
            if monkey.name in expression:
                expression = expression.replace(monkey.name, monkey.get_expression())
                updated = True
    return expression


def part_1(monkeys):
    expression = f"{monkeys['root'].waiting[0]} {monkeys['root'].operation} {monkeys['root'].waiting[1]}"
    expression = update_expression(monkeys, expression)
    return int(eval(expression))


def part_2(monkeys):
    left_part = monkeys["root"].waiting[0]
    right_part = monkeys["root"].waiting[1]
    monkeys["humn"].value = sympy.symbols("me")
    left_part = update_expression(monkeys, left_part)
    right_part = update_expression(monkeys, right_part)
    return sympy.solve(f"{left_part} - {right_part}")[0]


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    monkeys = {}
    for line in lines:
        new_monkey = Monkey(line)
        monkeys[new_monkey.name] = new_monkey

    if args.part == 1:
        result = part_1(monkeys)
        print(f"part1 results in {result}")
    else:
        result = part_2(monkeys)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
