import argparse
import os


DIRECTIONS = {
    "<": [-1, 0],
    ">": [1, 0],
    "v": [0, 1],
    "^": [0, -1],
}


class Blizard:
    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.direction = direction

    def move_1_minute(self, min_x, max_x, min_y, max_y):
        movement = DIRECTIONS[self.direction]
        self.x += movement[0]
        if self.x < min_x:
            self.x = max_x
        if self.x > max_x:
            self.x = min_x
        self.y += movement[1]
        if self.y < min_y:
            self.y = max_y
        if self.y > max_y:
            self.y = min_y


class BlizardMap:
    def __init__(self, blizard_map, all_blizards):
        self.blizard_map = blizard_map
        self.all_blizards = all_blizards
        self.min_x = 1
        self.max_x = len(self.blizard_map[0]) - 2
        self.min_y = 1
        self.max_y = len(self.blizard_map) - 2

    def __repr__(self):
        retval = ""
        for y, line in enumerate(self.blizard_map):
            retline = ""
            for x, char in enumerate(line):
                blizards = self.get_blizards_at(x, y)
                if len(blizards) == 1:
                    char = blizards[0].direction
                elif len(blizards) > 1:
                    char = f"{len(blizards)}"
                retline += char
            retval += f"{retline}\n"
        return retval

    def print_available(self):
        for line in self.blizard_map:
            print("".join(char for char in line))
        print()

    def get_blizards_at(self, x, y):
        blizards = []
        for blizard in self.all_blizards:
            if blizard.x == x and blizard.y == y:
                blizards.append(blizard)
        return blizards

    def clear_map(self):
        for x in range(self.min_x, self.max_x + 1):
            for y in range(self.min_y, self.max_y + 1):
                self.blizard_map[y][x] = "."

    def move_1_minute(self):
        self.clear_map()
        for blizard in self.all_blizards:
            blizard.move_1_minute(self.min_x, self.max_x, self.min_y, self.max_y)
            self.blizard_map[blizard.y][blizard.x] = " "

    def check_free(self, x, y):
        if 0 <= x <= self.max_x + 1 and 0 <= y <= self.max_y + 1:
            if self.blizard_map[y][x] == ".":
                return True
        return False


WALKER_DIRS = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
]


class Walker:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def __repr__(self):
        return f"Walker at {self.x},{self.y}"

    def move(self, blizard_map):
        next_walkers = []
        if blizard_map.check_free(self.x, self.y):
            new_walker = Walker(self.x, self.y)
            next_walkers.append(new_walker)
        for walk_dir in WALKER_DIRS:
            if blizard_map.check_free(self.x + walk_dir[0], self.y + walk_dir[1]):
                new_walker = Walker(self.x + walk_dir[0], self.y + walk_dir[1])
                next_walkers.append(new_walker)
        return next_walkers


def walk(blizard_map, from_pos, end_pos):
    count = 0
    walkers = [Walker(from_pos[0], from_pos[1])]
    while True:
        blizard_map.move_1_minute()
        new_walkers = []
        for walker in walkers:
            new_walkers.extend(walker.move(blizard_map))
        # insert a new walker at start as we do not allow going back there
        new_walkers.append(Walker(from_pos[0], from_pos[1]))
        walkers = []
        # remove duplicate walkers
        _ = [walkers.append(x) for x in new_walkers if x not in walkers]
        count += 1
        for walker in walkers:
            if walker.x == end_pos[0] and walker.y == end_pos[1]:
                # this walker reached the end
                return count
    return None


def part_1(blizard_map):
    return walk(blizard_map, [1, 0], [blizard_map.max_x, blizard_map.max_y + 1])


def part_2(blizard_map):
    time = walk(blizard_map, [1, 0], [blizard_map.max_x, blizard_map.max_y + 1])
    time += walk(blizard_map, [blizard_map.max_x, blizard_map.max_y + 1], [1, 0])
    time += walk(blizard_map, [1, 0], [blizard_map.max_x, blizard_map.max_y + 1])
    return time


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    blizard_map = []
    all_blizards = []
    for y, line in enumerate(lines):
        line = line.strip()
        blizard_map_line = []
        for x, char in enumerate(line):
            if char in DIRECTIONS:
                blizard = Blizard(x, y, char)
                all_blizards.append(blizard)
                char = "."
            blizard_map_line.append(char)
        blizard_map.append(blizard_map_line)

    blizard_map = BlizardMap(blizard_map, all_blizards)

    print(blizard_map)

    if args.part == 1:
        result = part_1(blizard_map)
        print(f"part1 results in {result}")
    else:
        result = part_2(blizard_map)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
