import argparse
import os


def check_unique(chars):
    for i, char_i in enumerate(chars):
        for j in range(i + 1, len(chars)):
            if char_i == chars[j]:
                return False
    return True


def find_unique_chars(line, unique_len=4):
    for char in range(len(line)-unique_len+1):
        substring = line[char:char+unique_len]
        if check_unique(substring):
            return char + unique_len
    return -1


def part_1(lines):
    result = []
    for line in lines:
        result.append(find_unique_chars(line.strip()))
    return result


def part_2(lines):
    result = []
    for line in lines:
        result.append(find_unique_chars(line.strip(), 14))
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
