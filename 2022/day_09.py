import argparse
import os
import re


HEAD = 0
MOVE_OFFSETS = {
    "U": [0, 1],
    "D": [0, -1],
    "L": [-1, 0],
    "R": [1, 0],
}


class Snake:
    def __init__(self, length):
        self.coordinates = [[0]*2 for _ in range(length)]
        self.positions_visited_tail = [["s"]]

    def __repr__(self):
        retval = ""
        for row in reversed(range(len(self.positions_visited_tail))):
            row_content = ""
            for column in range(len(self.positions_visited_tail[0])):
                for coordinate_nr, coordinate in enumerate(self.coordinates):
                    if coordinate[0] == column and coordinate[1] == row:
                        if coordinate_nr == 0:
                            row_content += "H"
                        else:
                            row_content += f"{coordinate_nr}"
                        break
                else:
                    row_content += self.positions_visited_tail[row][column]
            retval += f"{row_content}\n"
        return retval

    def add_column(self, right):
        for row in self.positions_visited_tail:
            if right:
                row.append(".")
            else:
                row.insert(0, ".")
        if not right:
            for coordinate in self.coordinates:
                coordinate[0] += 1

    def add_row(self, top):
        new_row = (["."] * len(self.positions_visited_tail[0]))
        if top:
            self.positions_visited_tail.append(new_row)
        else:
            self.positions_visited_tail.insert(0, new_row)
            for coordinate in self.coordinates:
                coordinate[1] += 1

    def move_head(self, move_dir):
        offset = MOVE_OFFSETS[move_dir]
        self.coordinates[HEAD][0] += offset[0]
        self.coordinates[HEAD][1] += offset[1]
        # extend area if needed
        if self.coordinates[HEAD][0] < 0:
            self.add_column(False)
        if self.coordinates[HEAD][1] < 0:
            self.add_row(False)
        if self.coordinates[HEAD][0] >= len(self.positions_visited_tail[0]):
            # increase horizontal size
            self.add_column(True)
        if self.coordinates[HEAD][1] >= len(self.positions_visited_tail):
            # increase vertical size
            self.add_row(True)

    def update_tail_pos(self, part2):
        for coordinate_nr in range(1, len(self.coordinates)):
            head_nr = coordinate_nr - 1
            tail_nr = coordinate_nr
            x_distance = abs(self.coordinates[head_nr][0] - self.coordinates[tail_nr][0])
            y_distance = abs(self.coordinates[head_nr][1] - self.coordinates[tail_nr][1])
            move_left = self.coordinates[head_nr][0] < self.coordinates[tail_nr][0]
            move_down = self.coordinates[head_nr][1] < self.coordinates[tail_nr][1]
            if (x_distance > 1 and y_distance >= 1) or (x_distance >= 1 and y_distance > 1):
                if move_left:
                    self.coordinates[tail_nr][0] -= 1
                else:
                    self.coordinates[tail_nr][0] += 1
                if move_down:
                    self.coordinates[tail_nr][1] -= 1
                else:
                    self.coordinates[tail_nr][1] += 1
            elif x_distance > 1:
                if move_left:
                    self.coordinates[tail_nr][0] -= 1
                else:
                    self.coordinates[tail_nr][0] += 1
            elif y_distance > 1:
                if move_down:
                    self.coordinates[tail_nr][1] -= 1
                else:
                    self.coordinates[tail_nr][1] += 1
        if part2:
            self.positions_visited_tail[self.coordinates[len(self.coordinates) - 1][1]][self.coordinates[len(self.coordinates) - 1][0]] = "#"
        else:
            for coordinate_nr in range(1, len(self.coordinates)):
                self.positions_visited_tail[self.coordinates[coordinate_nr][1]][self.coordinates[coordinate_nr][0]] = "#"

    def count_visited_tail(self):
        visited = 0
        for row in self.positions_visited_tail:
            for column in row:
                if column == "#":
                    visited += 1
        return visited


def part_1(moves):
    snake = Snake(2)
    for move in moves:
        move_dir = list(move.keys())[0]
        move_len = list(move.values())[0]
        for _ in range(move_len):
            snake.move_head(move_dir)
            snake.update_tail_pos(False)
    print(snake)
    return snake.count_visited_tail()


def part_2(moves):
    snake = Snake(10)
    for move in moves:
        move_dir = list(move.keys())[0]
        move_len = list(move.values())[0]
        for _ in range(move_len):
            snake.move_head(move_dir)
            snake.update_tail_pos(True)
    print(snake)
    return snake.count_visited_tail()


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    moves = []
    for line in lines:
        line = line.strip()
        line_re = re.search(r"([RULD]) (\d*)", line)
        moves.append({line_re.group(1): int(line_re.group(2))})

    if args.part == 1:
        result = part_1(moves)
        print(f"part1 results in {result}")
    else:
        result = part_2(moves)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
