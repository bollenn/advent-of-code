import argparse
import os


def check_command(line, currdir):
    info = line.split(" ")
    command = info[1]
    if command == "cd":
        dirname = info[2]
        if dirname == "/":
            currdir = dirname
        else:
            currdir = currdir + "/" + dirname
        currdir = currdir.replace("//", "/")
        currdir = os.path.abspath(currdir)
    return currdir


def check_dirlist(filetree, line, currdir):
    info = line.split(" ")
    size = info[0]
    if size != "dir":
        size = int(size)
    else:
        size = 0
    if currdir not in filetree:
        filetree[currdir] = 0
    filetree[currdir] += size


def calculate_dirsize(dirtree):
    dirs = {}
    for directory, dirsize in dirtree.items():
        if directory not in dirs:
            dirs[directory] = 0
        dirs[directory] += dirsize
    dirs_total = {}
    for dirname, dirsize in dirs.items():
        for dirname_sub, dirsize_sub in dirs.items():
            if dirname != dirname_sub:
                if dirname_sub.startswith(dirname):
                    dirsize += dirsize_sub
        dirs_total[dirname] = dirsize
    return dirs, dirs_total


def find_large(dirtree):
    total = 0
    for dirsize in dirtree.values():
        if dirsize <= 100000:
            total += dirsize
    return total


def interpret_filesystem(lines):
    filetree = {}
    current_dir = ""
    for line in lines:
        line = line.strip()
        if line.startswith("$"):
            # interpret command
            current_dir = check_command(line, current_dir)
        else:
            # interpret ls
            check_dirlist(filetree, line, current_dir)
    return calculate_dirsize(filetree)


def part_1(lines):
    _, dirs_total = interpret_filesystem(lines)
    return find_large(dirs_total)


def part_2(lines):
    disk_size = 70000000
    free_req = 30000000
    _, dirs_total = interpret_filesystem(lines)
    space_needed = free_req - (disk_size - dirs_total["/"])
    dirs_to_delete = []
    for dir_size in dirs_total.values():
        if dir_size >= space_needed:
            dirs_to_delete.append(dir_size)
    dirs_to_delete.sort()
    return dirs_to_delete[0]


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
