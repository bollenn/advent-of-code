import argparse
import os


def convert_to_prio(compart_letters):
    result = []
    for letter in compart_letters:
        prio = ord(letter) - ord("a") + 1
        if prio < 0:
            prio = ord(letter) - ord("A") + 1 + 26
        result.append(prio)
    return result


def find_duplicates(prio1, prio2):
    duplicates = []
    for prio in prio1:
        if prio in prio2:
            duplicates.append(prio)
    duplicates.sort()
    return duplicates[-1:][0]


def find_duplicates_v2(prio1, prio2, prio3):
    duplicates = []
    for prio in prio1:
        if prio in prio2:
            if prio in prio3:
                duplicates.append(prio)
    duplicates.sort()
    return duplicates[-1:][0]


def part_1(lines):
    prio_total = 0
    for line in lines:
        # split in 2
        line = line.strip()
        half = int(len(line) / 2)
        compart1 = line[:half]
        compart2 = line[half:]
        prio1 = convert_to_prio(compart1)
        prio2 = convert_to_prio(compart2)
        prio_sum = find_duplicates(prio1, prio2)
        prio_total += prio_sum
    return prio_total


def part_2(lines):
    new_lines = []
    new_line = []
    count = 0
    for line in lines:
        line = line.strip()
        count += 1
        new_line.append(line)
        if count >= 3:
            count = 0
            new_lines.append(new_line)
            new_line = []

    prio_total = 0
    for line in new_lines:
        # split in 3
        prio1 = convert_to_prio(line[0])
        prio2 = convert_to_prio(line[1])
        prio3 = convert_to_prio(line[2])
        prio_sum = find_duplicates_v2(prio1, prio2, prio3)
        prio_total += prio_sum
    return prio_total


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    if args.part == 1:
        result = part_1(lines)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
