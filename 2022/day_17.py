import argparse
import copy
import os


class Rock:
    def __init__(self, shape):
        self.shape = shape

    def __repr__(self):
        retval = ""
        for row in self.shape:
            row_text = ""
            for column in row:
                row_text += "#" if column == 1 else "."
            retval += f"{row_text}\n"
        return retval

    @property
    def height(self):
        return len(self.shape)

    @property
    def width(self):
        return len(self.shape[0])

    def check_hit(self, room, rock_pos, next_pos):
        for row_nr, row in enumerate(self.shape):
            for col_nr, col_char in enumerate(row):
                if col_char == 1:
                    if room.chamber[row_nr+next_pos[1]] & (0x01 << (col_nr+next_pos[0])) != 0:
                        return rock_pos
        return next_pos


class Room:
    def __init__(self, winds):
        self.chamber = [0, 0x7f]
        self.winds = winds
        self.wind_ctr = 0

    def __repr__(self):
        retval = ""
        for row in self.chamber:
            row_text = ""
            for bit in range(7):
                row_text += "." if (row & (1 << bit)) == 0 else "#"
            retval += f"{row_text}\n"
        return retval

    def find_top_of_pile(self):
        for line_nr, line in enumerate(self.chamber):
            if line != 0:
                return line_nr
        return None

    def drop_rock(self, rock):
        # increase chamber to fit rock
        to_insert = 3 - self.find_top_of_pile() + rock.height
        if to_insert >= 0:
            for _ in range(to_insert):
                self.chamber.insert(0, 0)
        else:
            for _ in range(abs(to_insert)):
                self.chamber.pop(0)

        # first coordinate, we keep track of rock left bottom
        rock_pos = [2, 0]
        moving = True
        while moving:
            next_pos = copy.copy(rock_pos)

            # apply wind
            wind_to_apply = self.winds[self.wind_ctr]
            if wind_to_apply == "<":
                next_pos[0] = max(0, next_pos[0]-1)
            elif wind_to_apply == ">":
                next_pos[0] = min(7, next_pos[0]+1+rock.width) - rock.width
            rock_pos = rock.check_hit(self, rock_pos, next_pos)

            # drop 1
            next_pos = copy.copy(rock_pos)
            next_pos[1] += 1
            next_pos = rock.check_hit(self, rock_pos, next_pos)

            # check if we've hit the floor
            if rock_pos[0] == next_pos[0] and rock_pos[1] == next_pos[1]:
                moving = False

            # prepare for next round
            rock_pos = next_pos
            self.wind_ctr += 1
            if self.wind_ctr >= len(self.winds):
                self.wind_ctr = 0
        # place stopped rock in the room
        for row_nr, row in enumerate(rock.shape):
            for col_nr, col_char in enumerate(row):
                if col_char == 1:
                    self.chamber[row_nr+rock_pos[1]] |= 0x01 << (col_nr+rock_pos[0])

    def find_pattern(self):
        for length in range(10, (len(self.chamber) - 1) // 2):
            if self.chamber[10:10+length] == self.chamber[10+length:10+2*length]:
                return length, self.chamber[10:10+length]
        return None, None

    def count_pattern(self, pattern):
        counting = True
        count = 0
        while counting:
            start = 10 + count * len(pattern)
            if self.chamber[start:start+len(pattern)] != pattern:
                return count
            count += 1


def part_x(rocks, winds, total_rocks):
    room = Room(winds)

    rock_ctr = 0
    pattern = None
    pattern_found_ctr = 0
    skipped_height = 0
    i = 0
    while i < total_rocks:
        rock_to_drop = rocks[rock_ctr]
        room.drop_rock(rock_to_drop)
        rock_ctr += 1
        if rock_ctr >= len(rocks):
            rock_ctr = 0
        if pattern is None:
            pattern_len, pattern_found = room.find_pattern()
            if pattern_len is not None:
                # pattern was found twice, keep some numbers
                pattern = pattern_found
                pattern_found_ctr = i
        else:
            count = room.count_pattern(pattern)
            if count == 3:
                # pattern was found 3 time, lets skipped some rocks to make things faster
                rock_diff = i - pattern_found_ctr
                skipped_height = ((total_rocks - i) // rock_diff) * len(pattern)
                i += ((total_rocks - i) // rock_diff) * rock_diff
        i += 1

    # return tower height
    return len(room.chamber) - room.find_top_of_pile() - 1 + skipped_height


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    winds = []
    for line in lines:
        line = line.strip()
        for wind in line:
            winds.append(wind)

    rocks = []
    # ####
    rocks.append(Rock([[1, 1, 1, 1]]))
    # .#.
    # ###
    # .#.
    rocks.append(Rock([[0, 1, 0], [1, 1, 1], [0, 1, 0]]))
    # ..#
    # ..#
    # ###
    rocks.append(Rock([[0, 0, 1], [0, 0, 1], [1, 1, 1]]))
    # #
    # #
    # #
    # #
    rocks.append(Rock([[1], [1], [1], [1]]))
    # ##
    # ##
    rocks.append(Rock([[1, 1], [1, 1]]))

    if args.part == 1:
        result = part_x(rocks, winds, 2022)
        print(f"part1 results in {result}")
    else:
        result = part_x(rocks, winds, 1000000000000)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
