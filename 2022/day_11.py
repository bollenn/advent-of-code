import argparse
import os
import re
import math


class Monkey:
    def __init__(self, monkey):
        self.monkey_nr = monkey[0]
        self.items = [int(item) for item in re.findall(r"\s*(\d{1,}),?", monkey[1])]
        self.operation = re.search(r"new =\s(.*)", monkey[2]).group(1)
        self.divisible = int(re.search(r"divisible by\s*(\d*)", monkey[3]).group(1))
        self.test_true = int(re.search(r"throw to monkey\s*(\d*)", monkey[4]).group(1))
        self.test_false = int(re.search(r"throw to monkey\s*(\d*)", monkey[5]).group(1))
        self.inspections = 0

    def __repr__(self):
        return f"monkey {self.monkey_nr} inspected {self.inspections} times"

    def inspect(self, monkeys, relief, relief_factor):
        while len(self.items):
            self.inspections += 1
            old = self.items.pop()
            worry = eval(self.operation)
            if relief:
                worry = worry // 3
            else:
                worry = worry % relief_factor
            if worry % self.divisible:
                monkeys[self.test_false].items.append(worry)
            else:
                monkeys[self.test_true].items.append(worry)


def get_monkey_business(monkeys, rounds, relief=True, relief_factor=None):
    for _ in range(rounds):
        for monkey in monkeys:
            monkey.inspect(monkeys, relief, relief_factor)
    inspections = []
    for monkey in monkeys:
        print(monkey)
        inspections.append(monkey.inspections)
    inspections.sort(reverse=True)
    return inspections[0] * inspections[1]


def part1(monkeys):
    return get_monkey_business(monkeys, 20)


def part2(monkeys):
    lcm = 1
    for monkey in monkeys:
        lcm = math.lcm(lcm, int(monkey.divisible))
    return get_monkey_business(monkeys, 10000, relief=False, relief_factor=lcm)


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    lines = ''.join(lines)
    re_matches = re.findall(r"Monkey\s*(\d*):\s*Starting items:\s*(.*)\n\s*Operation:\s*(.*)\n\s*Test:\s*(.*)\n\s*If true:\s*(.*)\n\s*If false:\s*(.*)", lines)

    monkeys = []
    for re_match in re_matches:
        monkeys.append(Monkey(re_match))

    if args.part == 1:
        result = part1(monkeys)
        print(f"part1 results in {result}")
    else:
        result = part2(monkeys)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
