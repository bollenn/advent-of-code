import argparse
import os
import re

# Test stacks
# [ ] [D] [ ]
# [N] [C] [ ]
# [Z] [M] [P]
#  1   2   3
test_stacks = [
  ["Z", "N"],
  ["M", "C", "D"],
  ["P"]
]

# Input stacks
# [S] [ ] [ ] [ ] [ ] [T] [Q] [ ] [ ]
# [L] [ ] [ ] [ ] [B] [M] [P] [ ] [T]
# [F] [ ] [S] [ ] [Z] [N] [S] [ ] [R]
# [Z] [R] [N] [ ] [R] [D] [F] [ ] [V]
# [D] [Z] [H] [J] [W] [G] [W] [ ] [G]
# [B] [M] [C] [F] [H] [Z] [N] [R] [L]
# [R] [B] [L] [C] [G] [J] [L] [Z] [C]
# [H] [T] [Z] [S] [P] [V] [G] [M] [M]
#  1   2   3   4   5   6   7   8   9
input_stacks = [
  ["H", "R", "B", "D", "Z", "F", "L", "S"],
  ["T", "B", "M", "Z", "R"],
  ["Z", "L", "C", "H", "N", "S"],
  ["S", "C", "F", "J"],
  ["P", "G", "H", "W", "R", "Z", "B"],
  ["V", "J", "Z", "G", "D", "N", "M", "T"],
  ["G", "L", "N", "W", "F", "S", "P", "Q"],
  ["M", "Z", "R"],
  ["M", "C", "L", "G", "V", "R", "T"]
]


def part_1(lines, stacks):
    for line in lines:
        # convert "move 1 from 2 to 1"
        line_re = re.search(r"move (\d*) from (\d*) to (\d*)", line)
        nr_to_move = int(line_re.group(1))
        move_from = int(line_re.group(2)) - 1
        move_to = int(line_re.group(3)) - 1
        for i in range(nr_to_move):
            stacks[move_to].append(stacks[move_from][-i - 1])
        stacks[move_from] = stacks[move_from][:-nr_to_move]
    result = ""
    for stack in stacks:
        result += f"{stack[-1]}"
    return result


def part_2(lines, stacks):
    for line in lines:
        # convert "move 1 from 2 to 1"
        line_re = re.search(r"move (\d*) from (\d*) to (\d*)", line)
        nr_to_move = int(line_re.group(1))
        move_from = int(line_re.group(2)) - 1
        move_to = int(line_re.group(3)) - 1
        stacks[move_to].extend(stacks[move_from][-nr_to_move:])
        stacks[move_from] = stacks[move_from][:-nr_to_move]
    result = ""
    for stack in stacks:
        result += f"{stack[-1]}"
    return result


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    stacks = test_stacks if args.test else input_stacks
    if args.part == 1:
        result = part_1(lines, stacks)
        print(f"part1 results in {result}")
    else:
        result = part_2(lines, stacks)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
