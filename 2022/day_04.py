import argparse
import os
import re


def part_1(elve_lists):
    overlaps = 0
    for elve in elve_lists:
        # check elve0 in 1
        elve0in1 = True
        for i in elve[0]:
            if i not in elve[1]:
                elve0in1 = False
                break
        # check elve1 in 0
        elve1in0 = True
        for i in elve[1]:
            if i not in elve[0]:
                elve1in0 = False
                break
        if elve0in1 or elve1in0:
            overlaps += 1
    return overlaps


def part_2(elve_lists):
    overlaps = 0
    for elve in elve_lists:
        overlap = False
        for i in elve[0]:
            if i in elve[1]:
                overlap = True
        if overlap:
            overlaps += 1
    return overlaps


def main():
    parser = argparse.ArgumentParser(description="Advent Of Code Helper")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help="whether test is running")
    parser.add_argument("part",
                        action="store",
                        type=int,
                        help="which part is running")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    day = os.path.basename(__file__).split('.')[0]
    input_file = f"./{'test' if args.test else 'input'}/{day}.txt"
    with open(input_file, "r", encoding="utf8") as infile:
        lines = infile.readlines()

    elve_lists = []
    for line in lines:
        # convert to list
        line_re = re.search(r"(\d*)-(\d*),(\d*)-(\d*)", line)
        elve_1 = list(range(int(line_re.group(1)), int(line_re.group(2)) + 1))
        elve_2 = list(range(int(line_re.group(3)), int(line_re.group(4)) + 1))
        elve_lists.append([elve_1, elve_2])

    if args.part == 1:
        result = part_1(elve_lists)
        print(f"part1 results in {result}")
    else:
        result = part_2(elve_lists)
        print(f"part2 results in {result}")


if __name__ == "__main__":
    main()
